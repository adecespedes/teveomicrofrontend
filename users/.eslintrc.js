module.exports = {
  plugins: ["simple-import-sort"],
  rules: {
    "simple-import-sort/imports": "warn",

    exclude: /node_modules/,
    loader: "eslint-loader",
  },
  extends: ["@nuxtjs"],
};
// {
//   root: true,
//   env: {
//     browser: true,
//     node: true,
//   },
//   parserOptions: {
//     parser: 'babel-eslint',
//   },
//   extends: [
//     '@nuxtjs',
//     // 'prettier',
//     'prettier/vue',
//     'plugin:prettier/recommended',
//     'plugin:nuxt/recommended',
//   ],
//   plugins: ['prettier'],
//   // add your custom rules here
//   rules: {},
// }
