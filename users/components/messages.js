const messages = {
  en: {
    load: 'Load',
    apply: 'Apply',
    upload: 'Upload',
    zoomIn: 'Zoom +',
    zoomOut: 'Zoom -',
    flipV: 'Flip vertical',
    flipH: 'Flip horizontal',
    center: 'Center',
    closeLabel: 'Close',
    saveLabel: 'Save',
    cancelButton: 'Cancel',
    deleteButton: 'Delete',
    clearSelection: 'Unselect',
    words: '{count} word | words {count}',
    required: 'is a required field',
    email: 'This is not a valid email',
    noData: 'Nothing here',
    resultData: '{count} suggestion found | {count} suggestions found',
    searchFor: 'Searching for "{param}"',
    Name: 'Name',
    Caption: 'Caption',
    Note: 'Allowed file types only includes: {params}',
    editMeta: 'Edit file metadata',
    editingMeta: 'Editing {file}',
    addMoreButton: '+ Add more',
    selectedFiles: '{n} file selected | {n} selected files',
    uppyFileNotAllowed: 'File not allowed!',
    uppyFileNotAllowedDetail:
      'File couldn’t be added to Uppy because do not match validations',
    uppyUploadNotAllowed: 'Upload not allowed!',
    uppyUploadNotAllowedDetail:
      'Files couldn’t be upload because do not match validations'
  },
  es: {
    load: 'Cargar',
    apply: 'Aplicar',
    upload: 'Subir',
    zoomIn: 'Zoom +',
    zoomOut: 'Zoom -',
    flipV: 'Voltear vertical',
    flipH: 'Voltear horizontal',
    center: 'Centrar',
    closeLabel: 'Cerrar',
    saveLabel: 'Guardar',
    cancelButton: 'Cancelar',
    deleteButton: 'Eliminar',
    clearSelection: 'Deseleccionar',
    words: '{count} palabra | palabras {count}',
    required: 'es un parámetro requerido',
    email: 'Este correo no es válido',
    noData: 'No hay datos',
    resultData:
      '{count} sugerencia encontrada | {count} sugerencias encontradas',
    searchFor: 'Buscando "{param}"',
    Name: 'Nombre',
    Caption: 'Etiqueta',
    Note: 'Los tipos de archivos aceptados son: {params}',
    errorFiltering: 'Error al filtrar: {error}',
    editMeta: 'Editar metadatos del archivo',
    editingMeta: 'Editando {file}',
    addMoreButton: '+ Agregar más',
    selectedFiles: '{n} archivo seleccionado | {n} archivos seleccionados',
    uppyFileNotAllowed: 'Archivo no permitido!',
    uppyFileNotAllowedDetail:
      'El archivo no puedo agregarse a Uppy porque no cumple las validaciones',
    uppyUploadNotAllowed: 'Subida de archivos bloqueada!',
    uppyUploadNotAllowedDetail:
      'Los archivos no pueden subirse porque no cumplen las validaciones'
  }
}

export default messages
