export default function ({ store, redirect }) {
  if (!store.state.auth.loggedIn) {
    try {
      redirect(201, '/')
    } catch (error) {
      // error
    }
  }
}
