export default ({ route, app, from, redirect }) => {
  const adminRequired = route.meta.some((meta) => meta.requiresAdmin)
  let isAdmin = false
  let roles = []
  if (app.$auth.$state.loggedIn) {
    //   let perms = []
    //   if (app.$auth.$state.user) {
    //     perms =
    //       (app.$auth.$state.user.userPermissions &&
    //         app.$auth.$state.user.userPermissions.systemPermissions) ||
    //       []
    //   }

    roles =
      (app.$auth.$state.user.userRolesAndPermissions &&
        app.$auth.$state.user.userRolesAndPermissions.userRoles) ||
      app.$auth.$state.user.roles

    let rolList = []
    if (roles && roles.length) rolList = roles.map((r) => r.name || r)

    isAdmin =
      rolList.includes('Administrador de sistema') ||
      rolList.includes('Administrador global') ||
      rolList.includes('Administrator')
    // isAdmin = perms.includes('AccessAll') || perms.includes('SystemAdmin')
  }
  if (
    (adminRequired && !isAdmin) ||
    route.name === 'video' ||
    route.name === 'audio' ||
    route.name === 'img' ||
    route.name === 'text'
  ) {
    try {
      redirect(201, '/forbidden')
    } catch (error) {
      // error
    }
  }
}
