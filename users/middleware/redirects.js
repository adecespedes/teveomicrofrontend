const redirects = require('../data/redirects.json') // update to your file path

export default function ({ params, route, redirect }) {
  // find the redirect if it exists where the from === the requested url
  const changeRoute = redirects.find((r) => r.from === route.path)

  // If it exists, redirect the page with a 301 response else carry on
  if (changeRoute) {
    redirect('301', changeRoute.new)
  }
}
