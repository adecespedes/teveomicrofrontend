const signalr = require('@microsoft/signalr')
// const dayjs = require('dayjs')

export default ({ app, store, $auth }) => {
  if (!$auth.loggedIn) {
    return
  }
  const connection = new signalr.HubConnectionBuilder()
    .withUrl(
      `${app.store.state.APP_BASE_URL}${app.store.state.API_DATA_PREFIX}/Notifications`
    )
    .configureLogging(signalr.LogLevel.Information)
    .build()

  connection.on('Connected', (message) => {
    // eslint-disable-next-line no-console
    console.info('Connected to SignalR Hub.', message)
    timeStartSuccess()
  })

  connection.on('Disconnected', (message) => {
    // eslint-disable-next-line no-console
    console.warn('Disconnected from SignalR Hub.', message)
  })

  // with reconnect capability (async/await, not IE11 compatible)
  function start() {
    connection
      .start()
      .then(() => {
        timeStartSuccess()
        // app.store.commit('defineAppSignalRConnection', connection)
      })
      .catch(function (err) {
        setTimeout(() => {
          start()
        }, 5000)
        // eslint-disable-next-line no-console
        console.log(err)
      })
  }

  connection.onclose(() => {
    // eslint-disable-next-line no-console
    console.log('Attempting OnClose')
    start()
  })

  function timeStartSuccess(timeout = null) {
    // setTimeout(
    //   () => {
    connection.invoke('GetCurrentTime')
    //   },
    //   timeout !== null ? timeout * 1000 : 1000
    // )
  }

  connection.on(
    'VideoMetadata',
    function (
      fileId,
      name,
      route,
      routeForVisualize,
      duration,
      resolution,
      acpectRatio,
      thumbnail
    ) {
      app.store.commit('updateMediaProgressInfo', {
        fileId,
        name,
        route,
        routeForVisualize,
        duration,
        resolution,
        acpectRatio,
        thumbnail
      })
    }
  )

  connection.on(
    'AudioMetadata',
    function (fileId, name, route, routeForVisualize, duration) {
      app.store.commit('updateMediaProgressInfo', {
        fileId,
        name,
        route,
        routeForVisualize,
        duration,
        resolution: null
      })
    }
  )

  connection.on(
    'ImageMetadata',
    function (
      fileId,
      name,
      route,
      routeForVisualize,
      resolution,
      acpectRatio,
      thumbnail
    ) {
      app.store.commit('updateMediaProgressInfo', {
        fileId,
        name,
        route,
        routeForVisualize,
        duration: 0,
        resolution,
        acpectRatio,
        thumbnail
      })
    }
  )

  connection.on('MediaNotification', function (fileId, resume, status) {
    app.store.commit('updateMediaConvertionInfo', { fileId, resume, status })
  })

  setTimeout(() => {
    start()
  }, 3000)
}
