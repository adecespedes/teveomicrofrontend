import Vue from 'vue'

// Vue.config.errorHandler = (err: Error, vm: Vue, info: string) => {
//   console.log('Logged in Vue global error handler', err, vm, info)
// }

Vue.config.errorHandler = (err, instance, info) => {
  // handle error, e.g. report to a service
  // eslint-disable-next-line no-console
  console.log('Logged in Vue global error handler', err, instance, info)
  // eslint-disable-next-line no-console
  console.log('Logged instance', instance)
}

window.onunhandledrejection = (event) => {
  // eslint-disable-next-line no-console
  console.log('Logged in window.onunhandledrejection', event)
}
