export default async function ({ $axios }) {
  try {
    const data = await $axios.get(
      `https://teveo.premiumtesh.nat.cu/config.json`
    );
    // Set axios configurations
    $axios.setBaseURL(`${data.data.API_DATA_PREFIX}`);
    // $axios.setBaseURL(`/api-data`)
    $axios.setHeader("Access-Control-Allow-Origin", "*");
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e.message);
  }
}
