import Vue from 'vue'
import Vuex from 'vuex'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from './components/nuxt-error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'
import { createStore } from './store.js'

/* Plugins */

import nuxt_plugin_plugin_8df4faa4 from 'nuxt_plugin_plugin_8df4faa4' // Source: .\\components\\plugin.js (mode: 'all')
import nuxt_plugin_plugin_5b16dc9c from 'nuxt_plugin_plugin_5b16dc9c' // Source: .\\composition-api\\plugin.mjs (mode: 'all')
import nuxt_plugin_pluginutils_02fb4286 from 'nuxt_plugin_pluginutils_02fb4286' // Source: .\\nuxt-i18n\\plugin.utils.js (mode: 'all')
import nuxt_plugin_pluginrouting_1d6630c8 from 'nuxt_plugin_pluginrouting_1d6630c8' // Source: .\\nuxt-i18n\\plugin.routing.js (mode: 'all')
import nuxt_plugin_pluginmain_3855afa7 from 'nuxt_plugin_pluginmain_3855afa7' // Source: .\\nuxt-i18n\\plugin.main.js (mode: 'all')
import nuxt_plugin_buefy_0ab80b22 from 'nuxt_plugin_buefy_0ab80b22' // Source: .\\buefy.js (mode: 'all')
import nuxt_plugin_workbox_0ed45ea6 from 'nuxt_plugin_workbox_0ed45ea6' // Source: .\\workbox.js (mode: 'client')
import nuxt_plugin_metaplugin_26206fa6 from 'nuxt_plugin_metaplugin_26206fa6' // Source: .\\pwa\\meta.plugin.js (mode: 'all')
import nuxt_plugin_iconplugin_1cd124be from 'nuxt_plugin_iconplugin_1cd124be' // Source: .\\pwa\\icon.plugin.js (mode: 'all')
import nuxt_plugin_axios_48d90bd1 from 'nuxt_plugin_axios_48d90bd1' // Source: .\\axios.js (mode: 'all')
import nuxt_plugin_deviceplugin_7114425c from 'nuxt_plugin_deviceplugin_7114425c' // Source: .\\device.plugin.js (mode: 'all')
import nuxt_plugin_globalerrorhandler_85522ff4 from 'nuxt_plugin_globalerrorhandler_85522ff4' // Source: ..\\plugins\\global-error-handler.js (mode: 'client')
import nuxt_plugin_premiumteshuilib_4af19750 from 'nuxt_plugin_premiumteshuilib_4af19750' // Source: ..\\plugins\\premium-tesh-ui-lib.js (mode: 'client')
import nuxt_plugin_aftereach_071c8daf from 'nuxt_plugin_aftereach_071c8daf' // Source: ..\\plugins\\after-each.js (mode: 'client')
import nuxt_plugin_axios_5659d192 from 'nuxt_plugin_axios_5659d192' // Source: ..\\plugins\\axios.js (mode: 'client')
import nuxt_plugin_meta_13052eae from 'nuxt_plugin_meta_13052eae' // Source: .\\composition-api\\meta.mjs (mode: 'all')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Object.defineProperty(Vue.prototype, '$subapp', {
  get() {
    const globalNuxt = this.$root.$options.$subapp
    if (process.client && !globalNuxt && typeof window !== 'undefined') {
      return window.$subapp
    }
    return globalNuxt
  },
  configurable: true
})

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"page","mode":"out-in","appear":true,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

const originalRegisterModule = Vuex.Store.prototype.registerModule

function registerModule (path, rawModule, options = {}) {
  const preserveState = process.client && (
    Array.isArray(path)
      ? !!path.reduce((namespacedState, path) => namespacedState && namespacedState[path], this.state)
      : path in this.state
  )
  return originalRegisterModule.call(this, path, rawModule, { preserveState, ...options })
}

async function createApp(ssrContext, config = {}) {
  const router = await createRouter(ssrContext, config)

  const store = createStore(ssrContext)
  // Add this.$router into store actions/mutations
  store.$router = router

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"meta":[{"hid":"charset","charset":"utf-8"},{"hid":"viewport","name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"mobile-web-app-capable","name":"mobile-web-app-capable","content":"yes"},{"hid":"apple-mobile-web-app-title","name":"apple-mobile-web-app-title","content":"teveo"},{"hid":"author","name":"author","content":"PremiumTesh"},{"hid":"og:type","name":"og:type","property":"og:type","content":"website"},{"hid":"og:title","name":"og:title","property":"og:title","content":"teveo"},{"hid":"og:site_name","name":"og:site_name","property":"og:site_name","content":"teveo"}],"link":[{"type":"text\u002Fcss","href":"https:\u002F\u002Fcdn.jsdelivr.net\u002Fnpm\u002F@mdi\u002Ffont@5.8.55\u002Fcss\u002Fmaterialdesignicons.min.css","rel":"preload","as":"style","onload":"this.rel='stylesheet'"},{"hid":"shortcut-icon","rel":"shortcut icon","href":"\u002F_nuxt\u002Ficons\u002Ficon_64x64.19b84e.png"},{"hid":"apple-touch-icon","rel":"apple-touch-icon","href":"\u002F_nuxt\u002Ficons\u002Ficon_512x512.19b84e.png","sizes":"512x512"},{"rel":"manifest","href":"\u002F_nuxt\u002Fmanifest.0b1fb43c.json","hid":"manifest"}],"style":[],"script":[],"title":"teveo","htmlAttrs":{"lang":"en"}},

    store,
    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  // Make app available into store via this.app
  store.app = app

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    store,
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  function inject(key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value
    // Add into context
    if (!app.context[key]) {
      app.context[key] = value
    }

    // Add into store
    store[key] = app[key]

    // Check if plugin not already installed
    const installKey = '__subapp_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue.prototype, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  // Inject runtime config as $config
  inject('config', config)

  if (process.client) {
    // Replace store state before plugins execution
    if (window.__SUBAPP__ && window.__SUBAPP__.state) {
      store.replaceState(window.__SUBAPP__.state)
    }
  }

  // Add enablePreview(previewData = {}) in context for plugins
  if (process.static && process.client) {
    app.context.enablePreview = function (previewData = {}) {
      app.previewData = Object.assign({}, previewData)
      inject('preview', previewData)
    }
  }
  // Plugin execution

  if (typeof nuxt_plugin_plugin_8df4faa4 === 'function') {
    await nuxt_plugin_plugin_8df4faa4(app.context, inject)
  }

  if (typeof nuxt_plugin_plugin_5b16dc9c === 'function') {
    await nuxt_plugin_plugin_5b16dc9c(app.context, inject)
  }

  if (typeof nuxt_plugin_pluginutils_02fb4286 === 'function') {
    await nuxt_plugin_pluginutils_02fb4286(app.context, inject)
  }

  if (typeof nuxt_plugin_pluginrouting_1d6630c8 === 'function') {
    await nuxt_plugin_pluginrouting_1d6630c8(app.context, inject)
  }

  if (typeof nuxt_plugin_pluginmain_3855afa7 === 'function') {
    await nuxt_plugin_pluginmain_3855afa7(app.context, inject)
  }

  if (typeof nuxt_plugin_buefy_0ab80b22 === 'function') {
    await nuxt_plugin_buefy_0ab80b22(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_workbox_0ed45ea6 === 'function') {
    await nuxt_plugin_workbox_0ed45ea6(app.context, inject)
  }

  if (typeof nuxt_plugin_metaplugin_26206fa6 === 'function') {
    await nuxt_plugin_metaplugin_26206fa6(app.context, inject)
  }

  if (typeof nuxt_plugin_iconplugin_1cd124be === 'function') {
    await nuxt_plugin_iconplugin_1cd124be(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_48d90bd1 === 'function') {
    await nuxt_plugin_axios_48d90bd1(app.context, inject)
  }

  if (typeof nuxt_plugin_deviceplugin_7114425c === 'function') {
    await nuxt_plugin_deviceplugin_7114425c(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_globalerrorhandler_85522ff4 === 'function') {
    await nuxt_plugin_globalerrorhandler_85522ff4(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_premiumteshuilib_4af19750 === 'function') {
    await nuxt_plugin_premiumteshuilib_4af19750(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_aftereach_071c8daf === 'function') {
    await nuxt_plugin_aftereach_071c8daf(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_axios_5659d192 === 'function') {
    await nuxt_plugin_axios_5659d192(app.context, inject)
  }

  if (typeof nuxt_plugin_meta_13052eae === 'function') {
    await nuxt_plugin_meta_13052eae(app.context, inject)
  }

  // Lock enablePreview in context
  if (process.static && process.client) {
    app.context.enablePreview = function () {
      console.warn('You cannot call enablePreview() outside a plugin.')
    }
  }

  // Wait for async component to be resolved first
  await new Promise((resolve, reject) => {
    // Ignore 404s rather than blindly replacing URL in browser
    if (process.client) {
      const { route } = router.resolve(app.context.route.fullPath)
      if (!route.matched.length) {
        return resolve()
      }
    }
    router.replace(app.context.route.fullPath, resolve, (err) => {
      // https://github.com/vuejs/vue-router/blob/v3.4.3/src/util/errors.js
      if (!err._isRouter) return reject(err)
      if (err.type !== 2 /* NavigationFailureType.redirected */) return resolve()

      // navigated to a different route in router guard
      const unregister = router.afterEach(async (to, from) => {
        if (process.server && ssrContext && ssrContext.url) {
          ssrContext.url = to.fullPath
        }
        app.context.route = await getRouteData(to)
        app.context.params = to.params || {}
        app.context.query = to.query || {}
        unregister()
        resolve()
      })
    })
  })

  return {
    store,
    app,
    router
  }
}

export { createApp, NuxtError }
