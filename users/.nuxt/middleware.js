const middleware = {}

middleware['guest'] = require('..\\middleware\\guest.js')
middleware['guest'] = middleware['guest'].default || middleware['guest']

middleware['navigation-guard-admin'] = require('..\\middleware\\navigation-guard-admin.js')
middleware['navigation-guard-admin'] = middleware['navigation-guard-admin'].default || middleware['navigation-guard-admin']

middleware['redirects'] = require('..\\middleware\\redirects.js')
middleware['redirects'] = middleware['redirects'].default || middleware['redirects']

export default middleware
