export const state = () => ({
  /* User */
  user: null,
  userEmail: null,
  userAvatar: null,

  // Token Expiration Date
  tokenExpiration: null,

  /* NavBar */
  isNavBarVisible: true,

  /* FooterBar */
  isFooterBarVisible: true,

  /* Aside */
  isAsideVisible: true,
  isAsideMobileExpanded: false,

  /* Dark mode */
  isDarkModeActive: false,

  /* Url base for api */
  APP_BASE_URL: '',
  API_DATA_PREFIX: '',
  API_IDENTITY_PREFIX: '',
  LOGO_PREFIX: '',

  /* Configuration Data */
  SERVER_DATE_FORMAT: 'MM/DD/YYYY hh:mm:ss',

  /* Configurable URLs */
  APP_HEALTH_URL: '',

  /* App language */
  appLocale: 'es',

  /* Locations */
  locationNodes: [],

  /* Selected Emission on panes in StepOutline */
  selectedEmission: null,

  /* Selected Template on panes */
  selectedTemplate: null,

  /* Selected Inf Event in StepOutline */
  selectedInfEvent: null,

  /* Inf Element on edition in StepOutline */
  infElement: null,

  /* Inf Element Types of the System */
  elementTypes: null,

  // SignalR connection instance
  signalRConnection: null,

  // Server Time
  serverTime: null,

  // Uploaded Media Progress Info
  mediaProgressInfo: null,

  // Uploaded Media Convertion Info
  mediaConvertionInfo: null,

  /* Timeline Departures of the System */
  timelineDepartures: null,

  /* Timeline Items(Inf Elements) */
  timelineItems: null,

  // Emission Timeline offset
  timelineOffset: 0,

  showToolsPanes: true,

  visiblePanes: [5, 6, 7, 8],

  visibleToolsPanes: [1, 2, 3, 4]
})

export const mutations = {
  /* A fit-them-all commit */
  basic(state, payload) {
    state[payload.key] = payload.value
  },

  /* Aside Mobile */
  asideMobileStateToggle(state, payload = null) {
    const htmlClassName = 'has-aside-mobile-expanded'

    let isShow

    if (payload !== null) {
      isShow = payload
    } else {
      isShow = !state.isAsideMobileExpanded
    }

    if (isShow) {
      document.documentElement.classList.add(htmlClassName)
    } else {
      document.documentElement.classList.remove(htmlClassName)
    }

    state.isAsideMobileExpanded = isShow
  },

  /* Dark Mode */
  darkModeToggle(state, payload = null) {
    const htmlClassName = 'is-dark-mode-active'

    state.isDarkModeActive = !state.isDarkModeActive

    if (state.isDarkModeActive) {
      document.documentElement.classList.add(htmlClassName)
    } else {
      document.documentElement.classList.remove(htmlClassName)
    }
  },

  /* Aside Mobile */
  defineApiUrl(state, payload = null) {
    state.APP_BASE_URL = payload
  },

  defineApiPrefix(state, payload = null) {
    state.API_DATA_PREFIX = payload
  },

  defineIdentityApiUrl(state, payload = null) {
    state.API_IDENTITY_PREFIX = payload
  },

  defineLogoUrl(state, payload = null) {
    state.LOGO_PREFIX = payload
  },

  defineServerDateFormat(state, payload = null) {
    state.SERVER_DATE_FORMAT = payload
  },

  defineAppHealthUrl(state, payload = null) {
    state.APP_HEALTH_URL = payload
  },

  defineAppLocale(state, payload = null) {
    state.appLocale = payload
  },

  defineAppUser(state, payload = null) {
    state.user = payload
  },

  defineAppUserEmail(state, payload = null) {
    state.userEmail = payload
  },

  defineAppUserAvatar(state, payload = null) {
    state.userAvatar = payload
  },

  defineTokenExpiration(state, payload = null) {
    state.tokenExpiration = payload
  },

  defineAppSignalRConnection(state, payload = null) {
    state.signalRConnection = payload
  },

  updateServerTime(state, payload = null) {
    state.serverTime = payload
  },

  updateMediaProgressInfo(state, payload = null) {
    state.mediaProgressInfo = payload
  },

  updateMediaConvertionInfo(state, payload = null) {
    state.mediaConvertionInfo = payload
  },

  updateLocationNodes(state, payload = null) {
    state.locationNodes = payload
  },

  updateSelectedEmission(state, payload = null) {
    state.selectedEmission = payload
  },

  updateSelectedTemplate(state, payload = null) {
    state.selectedTemplate = payload
  },

  updateSelectedInfEvent(state, payload = null) {
    state.selectedInfEvent = payload
  },

  updateInfElement(state, payload = null) {
    state.infElement = payload
  },

  updateElementTypes(state, payload = null) {
    state.elementTypes = payload
  },

  updateTimelineDepartures(state, payload = null) {
    state.timelineDepartures = payload
  },

  updateTimelineItems(state, payload = null) {
    state.timelineItems = payload
  },

  updatePanelsState(state, payload = null) {
    state.showToolsPanes = payload
  },

  updateVisibleTools(state, payload = null) {
    state.visibleToolsPanes = payload
  },

  updateVisiblePanes(state, payload = null) {
    state.visiblePanes = payload
  },
  updateTimelineOffset(state, payload = null) {
    state.timelineOffset = payload
  }
}

export const getters = {
  baseApiUrl(state) {
    return state.APP_BASE_URL
  },

  baseIdentityApiUrl(state) {
    return state.API_IDENTITY_PREFIX
  },

  baseLogoUrl(state) {
    return state.LOGO_PREFIX
  },

  baseServerDateFormat(state) {
    return state.SERVER_DATE_FORMAT
  },

  appHealthUrl(state) {
    return state.APP_HEALTH_URL
  }
}
