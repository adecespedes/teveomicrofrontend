export default {
  computed: {
    ctxOptions() {
      const opt = [
        { id: 'add', icon: 'plus', name: this.$t('addButton') },
        { id: 'edit', icon: 'pencil', name: this.$t('editButton') },
        { id: 'delete', icon: 'trash-can', name: this.$t('deleteButton') }
      ]
      return this.rowInCtx ? opt : opt.slice(0, 1)
    }
  },
  data() {
    return {
      rowInCtx: null,
      ctxOpen: false
    }
  },
  methods: {
    handleCtxMenuClicked(option, record) {
      if (option && this.$refs.table) {
        if (option.id === 'add') this.$refs.table.formOpen(null)
        else if (option.id === 'edit' && record)
          this.$refs.table.formOpen(record[this.$refs.table.idName])
        else if (option.id === 'delete' && record)
          this.$refs.table.deleteRecord(
            record[this.$refs.table.idName],
            record[this.$refs.table.rowName]
          )
      }
    }
  },
  provide() {
    return { child: this }
  },
  components: {
    ctx: {
      inject: ['child'],
      template: `<context-menu
        id="table-ctx-menu"
        :options="child.ctxOptions"
        :open="child.ctxOpen"
        @ctx-close="child.ctxOpen = false"
        @ctx-cancel="
          () => {
            child.rowInCtx = null
            child.ctxOpen = false
          }
        "
        @optionClicked="(opt) => child.handleCtxMenuClicked(opt, child.rowInCtx)"
      />`
    }
  }
}
