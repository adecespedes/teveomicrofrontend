import {
  AutocompleteInput,
  DateTimeComponent,
  NumberComponent,
  SelectComponent,
  SwitchComponent,
  TextComponent
} from 'premium-tesh-ui-lib'

export default {
  data() {
    return {
      streamTargetsForm: {
        id: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        streamTargetName: {
          type: TextComponent,
          value: '',
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('nameLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('nameLabel').toLowerCase()
            })
          }
        },
        incomingStreamId: {
          type: AutocompleteInput,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$tc('incomingStreamLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$tc('incomingStreamLabel').toLowerCase()
            }),
            optionId: 'incomingStreamId',
            optionName: 'incomingStreamName',
            allowAdd: false,
            openOnFocus: true,
            dataUrl: `contentchannel/${this.contentChannelId}/IncomingStream`
          },
          save: (v) => {
            return v ? v.incomingStreamId || v.id : v
          }
        },
        incomingStreamName: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          },
          render: (v) => {
            if (v)
              this.streamTargetsForm.incomingStreamId.value = {
                id: this.streamTargetsForm.incomingStreamId.value,
                incomingStreamId: this.streamTargetsForm.incomingStreamId.value,
                displayName: v,
                incomingStreamName: v
              }
          }
        },
        socialMediaAccountRtmpId: {
          type: AutocompleteInput,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            isRequired: true,
            label: this.$tc('rtmpAccount'),
            expanded: true,
            placeholder: this.$t('placeholderSelect', {
              key: this.$tc('rtmpAccount').toLowerCase()
            }),
            optionId: 'id',
            optionName: 'name',
            dataUrl: `contentChannel/${this.contentChannelId}/SocialMediaAccountForRtmp`,
            allowAdd: false,
            openOnFocus: true
          },
          save: (v) => {
            return v ? v.id : v
          }
        },
        socialMediaName: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          },
          render: (v) => {
            if (v)
              this.streamTargetsForm.socialMediaAccountRtmpId.value = {
                id: this.streamTargetsForm.socialMediaAccountRtmpId.value,
                name: v
              }
          }
        },
        enabled: {
          type: SwitchComponent,
          default: '',
          'field-wrapper': { class: 'is-12', style: 'display: none' },
          'field-input': {
            isRequired: false,
            label: this.$t('enabledLabel'),
            trueObject: {
              value: true,
              label: '',
              tooltip: this.$t('enabledLabel')
            },
            falseObject: {
              value: false,
              tooltip: this.$t('disabledLabel')
            }
          },
          save: (v) => {
            if (v === true) {
              return true
            } else {
              return false
            }
          }
        }
      },
      incomingStreamAssociationForm: {
        videoStreamId: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        incomingStreamId: {
          type: AutocompleteInput,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            isRequired: true,
            label: this.$tc('incomingStreamLabel'),
            expanded: true,
            placeholder: this.$t('placeholderSelect', {
              key: this.$tc('incomingStreamLabel').toLowerCase()
            }),
            optionId: 'id',
            optionName: 'name',
            dataUrl: `IncomingStream`,
            allowAdd: false,
            openOnFocus: true
          },
          save: (v) => {
            return v ? v.id : v
          }
        }
      },
      autoTransmitionForm: {
        id: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        streamTargetId: {
          type: null,
          default: '',
          'field-wrapper': { class: 'is-12', style: 'display: none' },
          save: (v) => {
            if (typeof v === 'object') return v.id
            return v
          }
        },
        transmissionDateTimeForEnable: {
          type: DateTimeComponent,
          default: null,
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('startTime'),
            placeholder: this.$t('placeholder', {
              key: this.$t('startTime').toLowerCase()
            }),
            minDateTime: new Date(),
            showClearButton: true,
            showNowButton: true,
            appendToBody: true,
            position: 'is-top-right'
          },
          save: (v) => {
            return v || null
          }
        },
        transmissionDateTimeForDisable: {
          type: DateTimeComponent,
          default: null,
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('endTime'),
            placeholder: this.$t('placeholder', {
              key: this.$t('endTime').toLowerCase()
            }),
            minDateTime: new Date(),
            showClearButton: true,
            showNowButton: true,
            appendToBody: true,
            position: 'is-top-right'
          },
          save: (v) => {
            return v || null
          }
        }
      }
    }
  },
  computed: {
    rtmpAccountsForm() {
      return {
        id: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        contentChannelId: {
          type: null,
          default: this.contentChannelId,
          'field-wrapper': { style: 'display: none' }
        },
        name: {
          type: TextComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('socialMediaNameLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$tc('socialMediaNameLabel').toLowerCase()
            })
          }
        },
        rtmpDestinationId: {
          type: SelectComponent,
          default: '---',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('destinationType'),
            placeholder: this.$t('placeholderSelect', {
              key: this.$tc('destinationType').toLowerCase()
            }),
            expanded: true,
            optionId: 'id',
            optionName: 'name',
            optionItems: this.rtmpDestinations
          },
          events: {
            input: (v) => {
              this.asiggnPreloadConfig(v)
              this.switchRTMPFields(v !== '---')
            }
          },
          save: (v) => {
            return v === '---' ? null : v
          }
        },
        rtmpDestinationName: {
          type: TextComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('destinationName'),
            placeholder: this.$t('placeholder', {
              key: this.$tc('destinationName').toLowerCase()
            })
          }
        },
        rtmpDestinationApplication: {
          type: TextComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('destinationAppLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('destinationAppLabel').toLowerCase()
            })
          }
        },
        rtmpDestinationHost: {
          type: TextComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('hostLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('hostLabel').toLowerCase()
            })
          }
        },
        rtmpDestinationPort: {
          type: NumberComponent,
          default: 0,
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            isRequired: true,
            label: this.$t('portLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('portLabel').toLowerCase()
            })
          }
        },
        rtmpDestinationUseSSL: {
          type: SwitchComponent,
          default: true,
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            isRequired: false,
            label: this.$t('useSSLLabel'),
            trueObject: {
              value: true,
              label: '',
              tooltip: this.$t('useSSLLabel')
            },
            falseObject: {
              value: false,
              label: '',
              tooltip: this.$t('useSSLLabel')
            }
          },
          save: (v) => {
            if (v === true) {
              return true
            } else {
              return false
            }
          }
        },
        streamKey: {
          type: TextComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('destinationStreamLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('destinationStreamLabel').toLowerCase()
            })
          }
        },
        socialMediaRanking: {
          type: NumberComponent,
          default: 1,
          'field-wrapper': { class: 'is-12', style: 'display: none' },
          'field-input': {
            label: this.$t('rankingLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('rankingLabel').toLowerCase()
            }),
            isRequired: false
          }
        }
      }
    }
  },
  watch: {
    'autoTransmitionForm.transmissionDateTimeForEnable.value'(newVal) {
      if (!newVal) {
        this.autoTransmitionForm.transmissionDateTimeForDisable[
          'field-input'
        ].isRequired = true
        this.autoTransmitionForm.transmissionDateTimeForEnable[
          'field-input'
        ].isRequired = false
      }
    },
    'autoTransmitionForm.transmissionDateTimeForDisable.value'(newVal) {
      if (!newVal) {
        this.autoTransmitionForm.transmissionDateTimeForEnable[
          'field-input'
        ].isRequired = true
        this.autoTransmitionForm.transmissionDateTimeForDisable[
          'field-input'
        ].isRequired = false
      }
    }
  },
  created() {
    if (Array.isArray(this.rtmpDestinations))
      this.rtmpDestinations.splice(0, 0, {
        id: '---',
        name: this.$t('custom')
      })
  },
  methods: {
    switchRTMPFields(state) {
      for (const key in this.rtmpAccountsForm) {
        if (Object.hasOwnProperty.call(this.rtmpAccountsForm, key)) {
          if (
            key !== 'rtmpDestinationId' &&
            key !== 'name' &&
            key !== 'socialMediaRanking' &&
            key !== 'streamKey'
          ) {
            const element = this.rtmpAccountsForm[key]
            element.readonly = state
          }
        }
      }
    },
    asiggnPreloadConfig(configId) {
      const cog = this.rtmpDestinations.find((item) => item.id === configId)

      for (const key in this.rtmpAccountsForm) {
        if (Object.hasOwnProperty.call(this.rtmpAccountsForm, key))
          if (
            key !== 'contentChannelId' &&
            key !== 'id' &&
            key !== 'name' &&
            key !== 'socialMediaRanking'
          ) {
            const element = this.rtmpAccountsForm[key]
            if (key === 'rtmpDestinationId')
              element.value =
                cog && cog.id !== '---' ? cog.id : element.value || null
            else {
              // Modify form key to match rtmp destination keys
              let customKey = key.replace('rtmpDestination', '')
              customKey = customKey.at(0).toLowerCase() + customKey.slice(1)

              if (cog && customKey in cog) {
                element.value = cog[customKey]
              } else if (element.value !== undefined) {
                continue
              } else if (element.default !== undefined) {
                element.value = element.default
              } else {
                element.value = null
              }
            }
          }
      }
    }
  }
}
