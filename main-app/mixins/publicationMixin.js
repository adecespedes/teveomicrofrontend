// eslint-disable-next-line simple-import-sort/imports
import {
  AdvancedFilterComponent,
  TextAreaComponent,
  TextComponent,
  TYPE_AUTOCOMPLETE,
  TYPE_FULLDATETIME,
  TYPE_NUMBER,
  TYPE_TEXT
} from 'premium-tesh-ui-lib'
import { mapGetters } from 'vuex'

import mediaMixin from './mediaMixin'
import AdvancedCropper from '~/components/Inputs/AdvancedCropper.vue'
import CategoryListInput from '~/components/Inputs/CategoryListInput.vue'
import UltraFileUploader from '~/components/Inputs/UltraFileUploader.vue'
import categoryMixin from '~/mixins/categoryMixin'

export default {
  mixins: [categoryMixin, mediaMixin],
  data() {
    return {
      filterForm: {
        PublicationFilter: {
          title: this.$t('cardTitle'),
          icon: 'television-classic',
          fields: {
            Id: {
              fieldType: TYPE_TEXT,
              label: 'Id',
              placeholder: this.$t('placeholder', {
                key: 'id'
              }),
              activeOperator: '=='
            },
            Tittle: {
              fieldType: TYPE_TEXT,
              label: this.$t('tittleLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('tittleLabel').toLowerCase()
              }),
              activeOperator: '=='
            },
            Description: {
              fieldType: TYPE_TEXT,
              label: this.$t('descriptionLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('descriptionLabel').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            },
            ContentChannelName: {
              fieldType: TYPE_AUTOCOMPLETE,
              label: this.$tc('contentChannelLabel', 1),
              placeholder: this.$t('placeholder', {
                key: this.$tc('contentChannelLabel').toLowerCase()
              }),
              props: {
                optionId: 'id',
                optionName: 'name',
                dataUrl: `ContentChannel`,
                isRequired: false
              },
              save: (v) => {
                return v ? v.id : v
              },
              operators: ['==', '@=*'],
              activeOperator: '=='
            }
          }
        },
        PropertiesFilter: {
          title: this.$t('propertiesLabel'),
          icon: 'newspaper-variant-outline',
          fields: {
            Likes: {
              fieldType: TYPE_NUMBER,
              default: 0,
              label: this.$t('likesLabel')
            },
            Dislikes: {
              fieldType: TYPE_NUMBER,
              default: 0,
              label: this.$t('dislikesLabel')
            },
            Visits: {
              fieldType: TYPE_NUMBER,
              default: 0,
              label: this.$t('visitsLabel')
            },
            Reproductions: {
              fieldType: TYPE_NUMBER,
              default: 0,
              label: this.$t('reproductions')
            },
            Viewing: {
              fieldType: TYPE_NUMBER,
              default: 0,
              label: this.$tc('areViewingLabel')
            }
          }
        },
        UsersFilter: {
          title: this.$t('users'),
          icon: 'account-group',
          form: {
            CreatedBy: {
              fieldType: TYPE_TEXT,
              label: this.$t('createdBy'),
              placeholder: this.$t('placeholder', {
                key: this.$t('createdBy').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            },
            ModifiedBy: {
              fieldType: TYPE_TEXT,
              label: this.$t('modifiedBy'),
              placeholder: this.$t('placeholder', {
                key: this.$t('modifiedBy').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            }
          }
        },
        DatesFilter: {
          title: this.$tc('date', 2),
          icon: 'calendar',
          form: {
            CreatedDate: {
              fieldType: TYPE_FULLDATETIME,
              label: this.$t('createDateTime'),
              placeholder: this.$t('placeholder', {
                key: this.$t('createDateTime').toLowerCase()
              }),
              value: null
            },
            ModifiedDate: {
              fieldType: TYPE_FULLDATETIME,
              label: this.$t('modifyDateTime'),
              placeholder: this.$t('placeholder', {
                key: this.$t('modifyDateTime').toLowerCase()
              }),
              value: null
            }
          }
        }
      },
      filters: '',
      filtersConstants: {
        Id: 'id',
        Tittle: 'title',
        Description: 'description',
        Dislikes: 'dislikes',
        Likes: 'likes',
        Visits: 'visitsCount',
        Viewing: 'areViewingCount',
        ContentChannelName: 'contentChannelId',
        Reproductions: 'reproductionsCount'
      },
      filtersCount: 0,
      categorySave: []
    }
  },
  created() {
    this.CategoryForm.contentChannelId.default = this.contentChannelId
  },
  computed: {
    ...mapGetters({ mediaInfo: 'mediaProgressInfo' }),
    PublicationForm() {
      return {
        id: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        contentChannelId: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: true
          },
          save: () => {
            return this.contentChannelId
          }
        },
        presentationImagePath: {
          type: AdvancedCropper,
          default: '',
          'field-wrapper': {
            class: 'is-12'
          },
          'field-input': {
            placeholder: this.$t('placeholder', {
              key: this.$t('thumbnail').toLowerCase()
            }),
            isRequired: true,
            expanded: true,
            aspectRatio: 16 / 9,
            imageRestriction: 'stencil',
            uploadWithUppy: true,
            basePath: this.$store.state.API_DATA_PREFIX
          },
          events: {
            'image-uploaded': (f) => {
              if (f)
                this.PublicationForm.presentationImagePath.value =
                  f.DisplayPath || f.displayPath
            },
            'change-foto': (f) => {
              this.PublicationForm.presentationImagePath.value = f
            }
          }
        },
        title: {
          type: TextComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('title'),
            placeholder: this.$t('placeholder', {
              key: this.$t('title').toLowerCase()
            })
          }
        },
        description: {
          type: TextAreaComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('descriptionLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('descriptionLabel').toLowerCase()
            }),
            rows: 1
          }
        },
        categories: {
          type: CategoryListInput,
          default: [],
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$tc('category', 2),
            placeholder: this.$t('placeholder', {
              key: this.$tc('category', 1)
            }),
            dataUrl:
              '/contentchannel/' +
              this.contentChannelId +
              '/ContentChannelCategory',
            appendToBody: true,
            isAutocomplete: true,
            field: 'name',
            openOnFocus: true,
            form: this.CategoryForm,
            isRequired: false,
            showButtonModal: true
          },
          render: (v) => {
            const cats = []
            if (v !== undefined && v !== null) {
              v.forEach((item) =>
                cats.push({
                  id: item.contentChannelCategoryId,
                  name: item.contentChannelCategoryName
                })
              )
            }
            return cats
          },
          save: (v) => {
            const cats = []
            if (v !== null) {
              v.forEach((item) =>
                cats.push({ contentChannelCategoryId: item.id })
              )
            }
            return cats
          }
        },
        media: {
          type: UltraFileUploader,
          default: null,
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            isRequired: true,
            label: this.$t('mediasLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('mediasLabel').toLowerCase()
            }),
            maxNumberOfFiles: 1,
            showClearIcon: false,
            optionId: 'id',
            optionName: 'mediaTitle',
            dataUrl: `contentchannel/${this.contentChannelId}/MediaContext1`,
            allowedFileTypes: ['video/*', 'audio/*'],
            tusEndpoint: '/api-data/files',
            filterModalWidth: 650,
            filtersDefs: {
              title: this.$t('mediasLabel'),
              isInModal: true,
              form: this.FilterFormMedia
            },
            filterResultsDefs: {
              title: this.$t('mediasLabel'),
              headerIcon: 'image',
              vtable: {
                headers: this.HeadersMedia.slice(0, -1)
              }
            },
            metaFieldsForm: {
              Name: {
                type: TextComponent,
                'field-wrapper': { class: 'is-12' },
                'field-input': {
                  isRequired: false,
                  label: this.$t('nameLabel'),
                  showClearIcon: true
                },
                operators: ['==', '@=*'],
                activeOperator: '=='
              },
              Description: {
                type: TextComponent,
                'field-wrapper': { class: 'is-12' },
                'field-input': {
                  isRequired: false,
                  label: this.$t('descriptionLabel'),
                  showClearIcon: true
                },
                operators: ['==', '@=*'],
                activeOperator: '=='
              },
              Category: {
                type: TextComponent,
                'field-wrapper': { class: 'is-12' },
                'field-input': {
                  isRequired: false,
                  label: this.$t('categoryLabel'),
                  showClearIcon: true
                },
                operators: ['==', '@=*'],
                activeOperator: '=='
              }
            },
            tooltipSave: this.$t('tooltipSaveMedia')
          },
          events: {
            'mediainfo-selected': (v) => {
              this.PublicationForm.mediaId.value = v.mediaEntityId
              this.PublicationForm.mediaTitle.value = v.mediaTitle
              this.PublicationForm.mediaDescription.value = v.mediaDescription
              this.PublicationForm.aspectRatio.value = v.Ratio
              this.PublicationForm.resolution.value = v.Resolution
              this.PublicationForm.duration.value = v.duration
              this.PublicationForm.mediaInfoId.value = v.mediaInfoId
              this.PublicationForm.mediaType.value = v.mediaType
            },
            'mediainfo-uploaded': (v) => {
              if (v && typeof v === 'object') {
                // eslint-disable-next-line no-console
                console.log('Publication mixin subscribe')
                // eslint-disable-next-line no-console
                console.log(v)
                // Suscribe to media notifications
                this.suscribeNotificationGoup(v.FileId)
                // Create local Media stored entity
                this.$axios
                  .post(
                    `contentchannel/${this.contentChannelId}/MediaContext1`,
                    {
                      mediaType: v.MediaType,
                      streamType: 1,
                      mediaTitle: v.Name,
                      mediaDescription: v.Description,
                      sourcePath: v.DisplayPath,
                      duration: v.Duration || 0,
                      resolution: v.Resolution,
                      aspectRatio: v.Ratio,
                      thumbnail: v.Thumbnail,
                      mediaInfoId: v.Id
                    }
                  )
                  .then((response) => {
                    this.PublicationForm.mediaId.value =
                      response.data.mediaEntityId
                    this.PublicationForm.mediaTitle.value = v.Name
                    this.PublicationForm.mediaDescription.value = v.Description
                    this.PublicationForm.aspectRatio.value = v.Ratio
                    this.PublicationForm.resolution.value = v.Resolution
                    this.PublicationForm.duration.value = v.Duration
                    this.PublicationForm.mediaInfoId.value = v.Id
                    this.PublicationForm.mediaType.value = v.MediaType
                  })
                  .catch((err) => {
                    this.toastError(err)
                  })
              }
            }
          },
          save: (v) => {
            return v.mediaTitle || v.Name || v
          }
        },
        mediaId: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          value: null
        },
        mediaInfoId: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          value: null
        },
        mediaTitle: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          render: (v) => {
            if (v) this.PublicationForm.media.value = { mediaTitle: v }
          },
          value: ''
        },
        duration: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        aspectRatio: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        resolution: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        mediaType: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        mediaDescription: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        }
      }
    }
  },
  watch: {
    mediaInfo(newVal) {}
  },
  methods: {
    goToPublication(id) {
      this.$router.push('/publication/' + id)
    },
    showFilterForm(field) {
      const ffield = field && field.useFilter ? field : null
      this.assignColumnFilterForm(ffield)

      this.$buefy.modal.open({
        parent: this,
        component: AdvancedFilterComponent,
        props: {
          form: ffield ? ffield.filterForm : this.filterForm,
          isInModal: true,
          filterType: 'publications',
          stickyInside: true,
          locale: this.$i18n.locale,
          cardTitle: this.$t('cardTitle'),
          cardSubtitle: this.$t('filter'),
          enableAdvancedSearch: false,
          modalWidth: ffield ? 500 : 850,
          simpleMode: !!ffield,
          utc: true
        },
        events: {
          'filters-updated': this.updateFilters,
          'count-results': (q) => this.countResults(q, this.getBasePath),
          'filters-count': (v) => (this.filtersCount = v)
        },
        hasModalCard: true,
        destroyOnHide: true,
        trapFocus: true
      })
    }
  }
}
