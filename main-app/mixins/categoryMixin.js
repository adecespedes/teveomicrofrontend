import { TextAreaComponent, TextComponent } from 'premium-tesh-ui-lib'

export default {
  data() {
    return {
      CategoryForm: {
        id: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        contentChannelId: {
          type: null,
          default: this.contentChannelId || '',
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        name: {
          type: TextComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('nameLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('nameLabel').toLowerCase()
            })
          }
        },
        description: {
          type: TextAreaComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('descriptionLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('descriptionLabel').toLowerCase()
            }),
            rows: 1,
            isRequired: false
          }
        }
      }
    }
  },
  created() {},
  methods: {}
}
