import { saveAs } from 'file-saver'

export default {
  methods: {
    async downloadRecord(dataUrl, filename, self) {
      const jsonContent = await this.getRecordContent(dataUrl, self)
      this.saveRecord(jsonContent, filename)
    },
    saveRecord(jsonContent, filename) {
      const blob = new Blob([jsonContent], {
        type: 'text/plain;charset=utf-8'
      })
      saveAs(blob, filename)
    },
    async getRecordContent(dataUrl, self) {
      if (self === undefined) {
        self = this
      }
      let jsonContent
      try {
        const { data } = await self.$axios.get(dataUrl)
        jsonContent = data.json
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log('Error mixin getRecordContent: ', e)
        jsonContent = {}
      }
      return jsonContent
    },
    downloadImage(basePath, imgPath, filename) {
      const xhr = new XMLHttpRequest()

      const url = `${basePath}${imgPath}`

      const index = imgPath.toString().lastIndexOf('.')
      const extension = imgPath.toString().substring(index)
      filename = filename + extension

      xhr.responseType = 'blob' // Set the response type to blob so xhr.response returns a blob
      xhr.open('GET', url, true)

      xhr.onreadystatechange = function () {
        if (xhr.readyState === xhr.DONE) {
          saveAs(xhr.response, filename)
        }
      }
      xhr.send() // Request is sent
    }
  }
}
