import {
  AdvancedFilterComponent,
  DynamicForm,
  NumberSelectMeasureComponent,
  NumberSelectBitrateComponent,
  SelectComponent,
  TYPE_FULLDATETIME,
  TYPE_TEXT,
  TimeComponent
} from 'premium-tesh-ui-lib'

import mediaMixin from './mediaMixin'
import categoryMixin from '~/mixins/categoryMixin'

export default {
  mixins: [categoryMixin, mediaMixin],
  data() {
    return {
      filterForm: {
        MediaFilter: {
          title: this.$t('mediasLabel'),
          icon: 'pump',
          fields: {
            Id: {
              fieldType: TYPE_TEXT,
              label: 'Id',
              placeholder: this.$t('placeholder', {
                key: 'id'
              }),
              activeOperator: '=='
            },
            MediaTitle: {
              fieldType: TYPE_TEXT,
              label: this.$t('nameLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('nameLabel').toLowerCase()
              }),
              activeOperator: '=='
            },
            MediaDescription: {
              fieldType: TYPE_TEXT,
              label: this.$t('descriptionLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('descriptionLabel').toLowerCase()
              }),
              activeOperator: '=='
            },
            MediaSourcePath: {
              fieldType: TYPE_TEXT,
              label: this.$t('sourceLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('sourceLabel').toLowerCase()
              }),
              activeOperator: '=='
            },
            MediaType: {
              type: SelectComponent,
              default: '',
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('typeMediaLabel'),
                expanded: true,
                placeholder: this.$t('placeholder', {
                  key: this.$t('typeMediaLabel').toLowerCase()
                }),
                optionId: 'id',
                optionName: 'name',
                optionItems: [
                  { id: 0, name: this.$tc('audio', 1) },
                  { id: 1, name: this.$tc('video', 1) },
                  { id: 2, name: this.$tc('imageLabel', 1) }
                ],
                showClearIcon: true
              }
            },
            StreamType: {
              type: SelectComponent,
              default: '',
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('typeStreamLabel'),
                expanded: true,
                placeholder: this.$t('placeholder', {
                  key: this.$t('typeStreamLabel').toLowerCase()
                }),
                optionId: 'id',
                optionName: 'name',
                optionItems: [
                  { id: 0, name: this.$t('streamed') },
                  { id: 1, name: this.$t('stored') }
                ],
                showClearIcon: true
              }
            },
            Size: {
              type: NumberSelectMeasureComponent,
              default: 0,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('sizeLabel'),
                placeholder: this.$t('placeholder', {
                  key: this.$t('sizeLabel').toLowerCase()
                }),
                controlsPosition: 'compact',
                controlsAlignment: 'center',
                showClearIcon: true,
                min: 0
              },
              operators: ['==', '<', '>'],
              activeOperator: '=='
            },
            Duration: {
              type: TimeComponent,
              value: '',
              default: '',
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('durationLabel'),
                placeholder: this.$t('placeholder', {
                  key: this.$t('durationLabel').toLowerCase()
                }),
                showClearButton: true,
                convertUnit: 'milliseconds'
              },
              operators: ['==', '<', '>'],
              activeOperator: '=='
            },
            Resolution: {
              fieldType: TYPE_TEXT,
              label: this.$t('resolutionLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('resolutionLabel').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            },
            Extension: {
              fieldType: TYPE_TEXT,
              label: this.$t('extension'),
              placeholder: this.$t('placeholder', {
                key: this.$t('extension').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            },
            Bitrate: {
              type: NumberSelectBitrateComponent,
              default: 0,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('bitrate'),
                placeholder: this.$t('placeholder', {
                  key: this.$t('bitrate').toLowerCase()
                }),
                controlsPosition: 'compact',
                controlsAlignment: 'center',
                showClearIcon: true,
                min: 0
              },
              operators: ['==', '<', '>'],
              activeOperator: '=='
            }
          }
        },
        UsersFilter: {
          title: this.$t('users'),
          icon: 'account-group',
          form: {
            CreatedBy: {
              fieldType: TYPE_TEXT,
              label: this.$t('createdBy'),
              placeholder: this.$t('placeholder', {
                key: this.$t('createdBy').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            },
            ModifiedBy: {
              fieldType: TYPE_TEXT,
              label: this.$t('modifiedBy'),
              placeholder: this.$t('placeholder', {
                key: this.$t('modifiedBy').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            }
          }
        },
        DatesFilter: {
          title: this.$tc('date', 2),
          icon: 'calendar',
          form: {
            CreatedDate: {
              fieldType: TYPE_FULLDATETIME,
              label: this.$t('createDateTime'),
              placeholder: this.$t('placeholder', {
                key: this.$t('createDateTime').toLowerCase()
              }),
              value: null
            },
            ModifiedDate: {
              fieldType: TYPE_FULLDATETIME,
              label: this.$t('modifyDateTime'),
              placeholder: this.$t('placeholder', {
                key: this.$t('modifyDateTime').toLowerCase()
              }),
              value: null
            }
          }
        }
      },
      filters: '',
      filtersConstants: {
        Id: 'id',
        MediaTitle: 'mediaTitle',
        StreamType: 'streamType'
      },
      filtersCount: 0,
      tries: 3,
      loading: false
    }
  },
  methods: {
    goToPublication(id) {
      this.$router.push('/publication/' + id)
    },
    showAddModal() {
      this.$buefy.modal.open({
        parent: this,
        component: DynamicForm,
        props: {
          form: this.categoryForm,
          isInModal: true,
          locale: this.$i18n.locale,
          cardTitle: this.$tc('categoryLabel'),
          cardSubtitle: this.$t('create', { element: '' }),
          cardWidth: 700,
          preventSubmit: false,
          editionMode: false,
          submitUrl:
            '/contentchannel/' +
            this.contentChannelId +
            '/ContentChannelCategory'
        },
        hasModalCard: true,
        destroyOnHide: true,
        trapFocus: true,
        canCancel: ['escape', 'x']
      })
    },
    showFilterForm(field) {
      const ffield = field && field.useFilter ? field : null
      this.assignColumnFilterForm(ffield)

      this.$buefy.modal.open({
        parent: this,
        component: AdvancedFilterComponent,
        props: {
          form: ffield ? ffield.filterForm : this.filterForm,
          isInModal: true,
          filterType: 'multimedia',
          stickyInside: true,
          locale: this.$i18n.locale,
          cardTitle: this.$t('mediasLabel'),
          cardSubtitle: this.$t('filter'),
          enableAdvancedSearch: false,
          modalWidth: ffield ? 500 : 850,
          simpleMode: !!ffield,
          utc: true
        },
        events: {
          'filters-updated': this.updateFilters,
          'count-results': (q) => this.countResults(q, this.getBasePath),
          'filters-count': (v) => (this.filtersCount = v)
        },
        hasModalCard: true,
        destroyOnHide: true,
        trapFocus: true
      })
    }
  }
}
