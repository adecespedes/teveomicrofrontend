import {
  BoolToTextColumnComponent,
  CheckboxPicker,
  DateColumnComponent,
  LargeTextColumnComponent,
  MultipleButtonsColumnComponent,
  MediaVisualizerColumnComponent,
  NumberComponent,
  RadioPicker,
  TextAreaComponent,
  TextComponent,
  TYPE_FULLDATETIME,
  TYPE_NUMBER,
  TYPE_TEXT,
  UnitSelectorNumberComponent,
  UrlComponent
} from 'premium-tesh-ui-lib'
import utilitiesMixin from './utilitiesMixin'
import AdvancedCropper from '~/components/Inputs/AdvancedCropper.vue'
import DynamicField from '~/components/Inputs/DynamicField.vue'
import SocialMediaField from '~/components/SocialMediaField.vue'

export default {
  mixins: [utilitiesMixin],
  data() {
    return {
      HeadersCChannel: [
        {
          text: 'Id',
          value: 'id',
          align: 'left',
          sortable: true,
          filter: {
            useFilter: true,
            isActive: false,
            field: 'id'
          }
        },
        {
          text: this.$t('logoLabel'),
          value: 'logoPath',
          align: 'left',
          sortable: false,
          render: {
            type: MediaVisualizerColumnComponent,
            props: {
              showDisplayButton: true,
              fallbackMimeType: 'application/x-mpegur',
              locale: this.$i18n.locale,
              basePath: `${this.$store.state.APP_BASE_URL}${this.$store.state.API_DATA_PREFIX}`,
              blankPageUrl: '/media-preview'
            },
            rendervalue: (v) => {
              return v
                ? {
                    title: '',
                    media: v,
                    preview: v || undefined
                  }
                : ''
            }
          },
          filter: {
            useFilter: false
          }
        },
        {
          text: this.$tc('thumbnail'),
          value: 'presentationImagePath',
          align: 'left',
          sortable: false,
          render: {
            type: MediaVisualizerColumnComponent,
            props: {
              showDisplayButton: true,
              fallbackMimeType: 'application/x-mpegur',
              locale: this.$i18n.locale,
              basePath: `${this.$store.state.APP_BASE_URL}${this.$store.state.API_DATA_PREFIX}`,
              blankPageUrl: '/media-preview'
            },
            rendervalue: (v) => {
              return v
                ? {
                    title: '',
                    media: v,
                    preview: v || undefined
                  }
                : ''
            }
          },
          filter: {
            useFilter: false
          }
        },
        {
          text: this.$t('nameLabel'),
          value: 'name',
          align: 'left',
          sortable: true,
          filter: {
            useFilter: true,
            isActive: false,
            field: 'name'
          }
        },
        {
          text: this.$t('descriptionLabel'),
          value: 'description',
          align: 'left',
          sortable: false,
          render: {
            type: LargeTextColumnComponent,
            props: {
              appendToBody: false,
              showDisplayButton: true,
              showCopyButton: true,
              content: this.$t('descriptionLabel'),
              locale: this.$i18n.locale,
              textLenght: 30,
              tooltipPosition: 'is-right'
            },
            rendervalue: (val) => {
              const tmp = document.createElement('DIV')
              tmp.innerHTML = val
              return tmp.textContent || tmp.innerText || ''
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'description'
          }
        },
        {
          text: this.$t('subcribersLabel'),
          value: 'subcribersCount',
          align: 'left',
          sortable: true,
          filter: {
            useFilter: true,
            isActive: false,
            field: 'subcribersCount'
          }
        },
        {
          text: this.$t('allow', {
            key: 'Likes'
          }),
          value: 'allowVisualizeLikes',
          align: 'left',
          sortable: true,
          render: {
            type: BoolToTextColumnComponent,
            props: {
              trueText: this.$t('trueText'),
              falseText: this.$t('falseText')
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'allowVisualizeLikes'
          }
        },
        {
          text: this.$t('allow', {
            key: this.$tc('comment', 3)
          }),
          value: 'allowVisualizeComments',
          align: 'left',
          sortable: true,
          render: {
            type: BoolToTextColumnComponent,
            props: {
              trueText: this.$t('trueText'),
              falseText: this.$t('falseText')
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'allowVisualizeComments'
          }
        },
        {
          text: this.$t('allow', {
            key: this.$tc('notification', 2)
          }),
          value: 'allowVisualizeSubscribeNotifications',
          align: 'left',
          sortable: true,
          render: {
            type: BoolToTextColumnComponent,
            props: {
              trueText: this.$t('trueText'),
              falseText: this.$t('falseText')
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'allowVisualizeSubscribeNotifications'
          }
        },
        {
          text: this.$t('allow', {
            key: this.$t('subcribersLabel')
          }),
          value: 'allowVisualizeSubscribe',
          align: 'left',
          sortable: true,
          render: {
            type: BoolToTextColumnComponent,
            props: {
              trueText: this.$t('trueText'),
              falseText: this.$t('falseText')
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'allowVisualizeSubscribe'
          }
        },
        {
          text: this.$t('createdBy'),
          value: 'createdBy',
          align: 'left',
          sortable: true,
          custom: true,
          rendervalue: (value) => {
            return value || '---'
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'createdBy'
          }
        },
        {
          text: this.$t('createDateTime'),
          value: 'createdDate',
          align: 'left',
          sortable: true,
          render: {
            type: DateColumnComponent,
            props: { serverFormat: this.$store.state.SERVER_DATE_FORMAT },
            rendervalue: (value) => {
              return value || '---'
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'createdBy'
          }
        },
        {
          text: this.$t('modifiedBy'),
          value: 'modifiedBy',
          align: 'left',
          sortable: true,
          custom: true,
          rendervalue: (value) => {
            return value || '---'
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'createdBy'
          }
        },
        {
          text: this.$t('modifyDateTime'),
          value: 'modifiedDate',
          align: 'left',
          sortable: true,
          render: {
            type: DateColumnComponent,
            props: { serverFormat: this.$store.state.SERVER_DATE_FORMAT },
            rendervalue: (value) => {
              return value || '---'
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'createdBy'
          }
        },
        {
          text: this.$t('configuration'),
          value: '',
          custom: true,
          align: 'left',
          render: {
            type: MultipleButtonsColumnComponent,
            props: {
              buttons: [
                {
                  icon: 'account-key-outline',
                  type: 'is-white',
                  tooltip: {
                    label: this.$tc('permission', 2),
                    type: 'is-white',
                    position: 'is-top'
                  }
                },
                {
                  icon: 'cog',
                  type: 'is-white',
                  tooltip: {
                    label: this.$t('configuration'),
                    type: 'is-white',
                    position: 'is-top'
                  }
                }
              ]
            },
            events: {
              button0Clicked: (record) => {
                this.showEditConfig(record, false)
              },
              button1Clicked: (record) => {
                this.showEditConfig(record, true)
              }
            }
          },
          sortable: false,
          filter: {
            useFilter: false
          }
        },
        {
          text: this.$t('actions'),
          value: '',
          fixed: true,
          sortable: false,
          cellClass: `${!this.isReadonly && 'span-flex'}`,
          action: true,
          filter: {
            useFilter: false
          }
        }
      ],
      FilterFormCChannel: {
        ContentChannelFilter: {
          title: this.$tc('cardTitle'),
          icon: 'movie',
          fields: {
            Id: {
              fieldType: TYPE_TEXT,
              label: 'Id',
              placeholder: this.$t('placeholder', {
                key: 'id'
              }),
              activeOperator: '=='
            },
            Name: {
              fieldType: TYPE_TEXT,
              label: this.$t('nameLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('nameLabel').toLowerCase()
              }),
              activeOperator: '=='
            },
            Description: {
              fieldType: TYPE_TEXT,
              label: this.$t('descriptionLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('descriptionLabel').toLowerCase()
              }),
              activeOperator: '=='
            }
          }
        },
        PropertiesFilter: {
          title: this.$t('propertiesLabel'),
          icon: 'account-group',
          form: {
            Subcribers: {
              fieldType: TYPE_NUMBER,
              label: this.$t('subcribersLabel'),
              placeholder: this.$t('placeholder', {
                key: this.$t('subcribersLabel').toLowerCase()
              }),
              activeOperator: '=='
            },
            AllowLikes: {
              type: RadioPicker,
              default: null,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                label: this.$t('allow', {
                  key: this.$t('likesLabel').toLowerCase()
                }),
                options: {
                  true: this.$t('trueText'),
                  false: this.$t('falseText'),
                  null: this.$t('anyLabel')
                },
                customClass: 'group-is-block'
              },
              save: (v) => {
                return v !== 'null' ? v : null
              }
            },
            AllowComments: {
              type: RadioPicker,
              default: null,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                label: this.$t('allow', {
                  key: this.$tc('comment', 3)
                }),
                options: {
                  true: this.$t('trueText'),
                  false: this.$t('falseText'),
                  null: this.$t('anyLabel')
                },
                customClass: 'group-is-block'
              },
              save: (v) => {
                return v !== 'null' ? v : null
              }
            },
            AllowNotifications: {
              type: RadioPicker,
              default: null,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                label: this.$t('allow', {
                  key: this.$tc('notification', 2)
                }),
                options: {
                  true: this.$t('trueText'),
                  false: this.$t('falseText'),
                  null: this.$t('anyLabel')
                },
                customClass: 'group-is-block'
              },
              save: (v) => {
                return v !== 'null' ? v : null
              }
            },
            AllowSubcribers: {
              type: RadioPicker,
              default: null,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                label: this.$t('allow', {
                  key: this.$t('subcribersLabel')
                }),
                options: {
                  true: this.$t('trueText'),
                  false: this.$t('falseText'),
                  null: this.$t('anyLabel')
                },
                customClass: 'group-is-block'
              },
              save: (v) => {
                return v !== 'null' ? v : null
              }
            }
          }
        },
        UsersFilter: {
          title: this.$t('users'),
          icon: 'account-group',
          form: {
            CreatedBy: {
              fieldType: TYPE_TEXT,
              label: this.$t('createdBy'),
              placeholder: this.$t('placeholder', {
                key: this.$t('createdBy').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            },
            ModifiedBy: {
              fieldType: TYPE_TEXT,
              label: this.$t('modifiedBy'),
              placeholder: this.$t('placeholder', {
                key: this.$t('modifiedBy').toLowerCase()
              }),
              operators: ['==', '@=*'],
              activeOperator: '=='
            }
          }
        },
        DatesFilter: {
          title: this.$tc('date', 2),
          icon: 'calendar',
          fields: {
            CreatedDate: {
              fieldType: TYPE_FULLDATETIME,
              label: this.$t('createDateTime'),
              placeholder: this.$t('placeholder', {
                key: this.$t('createDateTime').toLowerCase()
              }),
              value: null
            },
            ModifiedDate: {
              fieldType: TYPE_FULLDATETIME,
              label: this.$t('modifyDateTime'),
              placeholder: this.$t('placeholder', {
                key: this.$t('modifyDateTime').toLowerCase()
              }),
              value: null
            }
          }
        }
      },
      SocialMediasForm: {
        contentChannelSocialMedias: {
          type: DynamicField,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: '',
            parameters: [
              {
                socialMedia: {
                  type: SocialMediaField,
                  default: '',
                  'field-wrapper': { class: 'is-2' },
                  'field-input': {
                    name: 'Facebook',
                    mdiIcon: 'facebook'
                  }
                },
                socialMediaAccount: {
                  type: UrlComponent,
                  default: '',
                  'field-wrapper': { class: 'is-10' },
                  'field-input': {
                    placeholder: this.$t('placeholder', {
                      key: this.$tc('account').toLowerCase()
                    }),
                    horizontal: false,
                    isRequired: false
                  }
                }
              },
              {
                socialMedia: {
                  type: SocialMediaField,
                  default: '',
                  'field-wrapper': { class: 'is-2' },
                  'field-input': {
                    name: 'Twitter',
                    mdiIcon: 'twitter'
                  }
                },
                socialMediaAccount: {
                  type: UrlComponent,
                  default: '',
                  'field-wrapper': { class: 'is-10' },
                  'field-input': {
                    placeholder: this.$t('placeholder', {
                      key: this.$tc('account').toLowerCase()
                    }),
                    horizontal: false,
                    isRequired: false
                  }
                }
              },
              {
                socialMedia: {
                  type: SocialMediaField,
                  default: '',
                  'field-wrapper': { class: 'is-2' },
                  'field-input': {
                    name: 'Instagram',
                    mdiIcon: 'instagram'
                  }
                },
                socialMediaAccount: {
                  type: UrlComponent,
                  default: '',
                  'field-wrapper': { class: 'is-10' },
                  'field-input': {
                    placeholder: this.$t('placeholder', {
                      key: this.$tc('account').toLowerCase()
                    }),
                    horizontal: false,
                    isRequired: false
                  }
                }
              },
              {
                socialMedia: {
                  type: SocialMediaField,
                  default: '',
                  'field-wrapper': { class: 'is-2' },
                  'field-input': {
                    name: 'Otras'
                  }
                },
                socialMediaAccount: {
                  type: UrlComponent,
                  default: '',
                  'field-wrapper': { class: 'is-10' },
                  'field-input': {
                    placeholder: this.$t('placeholder', {
                      key: this.$tc('account').toLowerCase()
                    }),
                    horizontal: false,
                    isRequired: false
                  }
                }
              }
            ]
          },
          save(newVal) {
            const v = {}
            for (const index in newVal) {
              v[index] = {}
              Object.keys(newVal[index]).forEach((field) => {
                if (field !== 'duplicated') {
                  v[index][field] = newVal[index][field].save
                    ? newVal[index][field].save(newVal[index][field].value)
                    : newVal[index][field].value
                }
              })
            }

            return JSON.stringify(v)
          },
          render: (v) => {
            if (v) {
              try {
                const json = JSON.parse(v)
                return json
              } catch (error) {
                return null
              }
            }
          },
          value: null
        }
      },
      //   For super admin edit Bandwith and Ranking
      SystemConfigForm: {
        ranking: {
          type: NumberComponent,
          default: null,
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            isRequired: false,
            label: this.$t('rankingLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('rankingLabel').toLowerCase()
            })
          }
        },
        channelExclusiveBandwidth: {
          type: UnitSelectorNumberComponent,
          default: null,
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            isRequired: false,
            label: this.$t('channelExclusiveBandwidthLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('channelExclusiveBandwidthLabel').toLowerCase()
            }),
            expanded: true,
            controls: false,
            units: ['b/s', 'kb/s', 'Mb/s', 'Gb/s', 'Tb/s'],
            baseUnit: 'b/s',
            converter: 'digital',
            unit: 'b/s',
            min: 0
          }
        }
      },
      contentChannelConfig: {
        type: CheckboxPicker,
        default: [
          'allowVisualizeLikes',
          'allowVisualizeComments',
          'allowVisualizeSubscribe',
          'allowVisualizeSubscribeNotifications'
        ],
        'field-wrapper': {
          class: 'is-12 group-is-block'
        },
        'field-input': {
          label: this.$tc('permission', 2),
          horizontal: false,
          options: {
            allowVisualizeComments: this.$t('allow', {
              key: this.$tc('comment', 2).toLowerCase()
            }),
            allowVisualizeLikes: this.$t('allow', { key: 'likes' }),
            allowVisualizeSubscribe: this.$t('allow', {
              key: this.$tc('subscribe', 2)
            }),
            allowVisualizeSubscribeNotifications: this.$t('allow', {
              key: this.$tc('subscribeNotify', 2)
            })
          }
        },
        save: (v) => {
          return {
            allowVisualizeLikes: v.length && v.includes('allowVisualizeLikes'),
            allowVisualizeComments:
              v.length && v.includes('allowVisualizeComments'),
            allowVisualizeSubscribe:
              v.length && v.includes('allowVisualizeSubscribe'),
            allowVisualizeSubscribeNotifications:
              v.length && v.includes('allowVisualizeSubscribeNotifications')
          }
        }
      }
    }
  },
  computed: {
    staticAudioPng() {
      return require('static/audio_media.png')
    },
    staticVideoPng() {
      return require('static/video_media.png')
    },
    cchannelCategoryUrl() {
      return this.reloadCategory
        ? ''
        : `/contentchannel/${
            this.contentChannel ? this.contentChannel.id : ''
          }/ContentChannelCategory`
    },
    Form() {
      return {
        id: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        logoPath: {
          type: AdvancedCropper,
          default: '',
          'field-wrapper': {
            class: 'is-12'
          },
          'field-input': {
            placeholder: this.$t('placeholder', {
              key: this.$t('logoLabel').toLowerCase()
            }),
            isRequired: false,
            expanded: true,
            stencilComponent: 'circle-stencil',
            aspectRatio: 16 / 9,
            uploadWithUppy: true,
            basePath: this.$store.state.API_DATA_PREFIX
          },
          events: {
            'image-uploaded': (f) => {
              if (f) {
                this.Form.logoPath.value = f.DisplayPath || f.displayPath || ''
                if (
                  this.Form.presentationImagePath.value &&
                  this.Form.presentationImagePath.value.split(';')[1] &&
                  this.Form.presentationImagePath.value
                    .split(';')[1]
                    .split(',')[0] === 'base64'
                ) {
                  this.Form.presentationImagePath.value =
                    this.contentChannel.presentationImagePath
                }
              }
            },
            'image-saved': () => {
              this.formBtns.save.show = true
            },
            'change-foto': (f) => {
              this.Form.logoPath.value = f
            }
          }
        },
        presentationImagePath: {
          type: AdvancedCropper,
          default: '',
          'field-wrapper': {
            class: 'is-12'
          },
          'field-input': {
            placeholder: this.$t('placeholder', {
              key: this.$tc('thumbnail').toLowerCase()
            }),
            isRequired: false,
            expanded: true,
            aspectRatio: 37 / 6,
            imageRestriction: 'none',
            uploadWithUppy: true,
            basePath: this.$store.state.API_DATA_PREFIX
          },
          events: {
            'image-uploaded': (f) => {
              if (f) {
                this.Form.presentationImagePath.value =
                  f.DisplayPath || f.displayPath || ''
                if (
                  this.Form.logoPath.value &&
                  this.Form.logoPath.value.split(';')[1] &&
                  this.Form.logoPath.value.split(';')[1].split(',')[0] ===
                    'base64'
                ) {
                  this.Form.logoPath.value = this.contentChannel.logoPath
                }
              }
            },
            'image-saved': () => {
              this.formBtns.save.show = true
            },
            'change-foto': (f) => {
              this.Form.presentationImagePath.value = f
            }
          }
        },
        name: {
          type: TextComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('nameLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('nameLabel').toLowerCase()
            })
          },
          events: {
            input: (text) => {
              if (this.contentChannel.name !== text)
                this.formBtns.save.show = true
              else this.formBtns.save.show = false
            }
          }
        },
        description: {
          type: TextAreaComponent,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            label: this.$t('descriptionLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('descriptionLabel').toLowerCase()
            }),
            rows: 1,
            isRequired: false
          },
          events: {
            input: (text) => {
              if (this.contentChannel.description !== text)
                this.formBtns.save.show = true
              else this.formBtns.save.show = false
            }
          }
        },
        ownerId: {
          type: TextComponent,
          default: (this.$auth.user && this.$auth.user.userName) || '',
          'field-wrapper': { class: 'is-12', style: 'display: none' },
          'field-input': {
            label: this.$t('ownerIdLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('ownerIdLabel').toLowerCase()
            }),
            isRequired: false
          }
        },
        ownerName: {
          type: TextComponent,
          default: (this.$auth.user && this.$auth.user.userName) || '',
          'field-wrapper': { class: 'is-12', style: 'display: none' },
          'field-input': {
            label: this.$t('ownerIdLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('ownerIdLabel').toLowerCase()
            }),
            isRequired: false
          }
        }
      }
    }
  },
  methods: {
    goToCChannel(id) {
      this.$router.push('/contentChannel/' + id)
    },
    getStaticMediaThumbnail(mediaType) {
      return mediaType === 0
        ? this.staticAudioPng
        : mediaType === 1
        ? this.staticVideoPng
        : ''
    }
  }
}
