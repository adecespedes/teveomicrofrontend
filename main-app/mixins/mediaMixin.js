import dayjs from 'dayjs'
import {
  BitrateColumnComponent,
  DateColumnComponent,
  DurationColumnComponent,
  FileSizeColumnComponent,
  FullDateTimePickerComponent,
  SelectComponent,
  TextComponent
} from 'premium-tesh-ui-lib'

import UppyFileUploader from '~/components/Inputs/UppyFileUploader.vue'
// import FileSizeColumnComponent from '~/components/FileSizeColumnComponent.vue'
// import DurationColumnComponent from '~/components/DurationColumnComponent.vue'
// import BitrateColumnComponent from '~/components/BitrateColumnComponent.vue'
const duration = require('dayjs/plugin/duration')
dayjs.extend(duration)

export default {
  data() {
    return {
      HeadersMedia: [
        {
          text: this.$t('titleLabel'),
          value: 'mediaTitle',
          align: 'left',
          sortable: true,
          filter: {
            useFilter: true,
            isActive: false,
            field: 'mediaTitle'
          }
        },
        {
          text: this.$t('typeMediaLabel'),
          value: 'mediaType',
          align: 'left',
          sortable: true,
          rendervalue: (value) => {
            return value === 1
              ? this.$tc('video', 1)
              : value === 0
              ? this.$tc('audio', 1)
              : this.$tc('imageLabel', 1)
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'mediaType'
          }
        },
        {
          text: this.$t('typeStreamLabel'),
          value: 'streamType',
          align: 'left',
          sortable: true,
          rendervalue: (value) => {
            return value === 1 ? this.$tc('stored') : this.$tc('streamed')
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'streamType'
          }
        },
        {
          text: this.$t('createdBy'),
          value: 'createdBy',
          align: 'left',
          sortable: true,
          custom: true,
          rendervalue: (value) => {
            return value || '---'
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'createdBy'
          }
        },
        {
          text: this.$t('createDateTime'),
          value: 'createdDate',
          align: 'left',
          sortable: true,
          render: {
            type: DateColumnComponent,
            props: { serverFormat: this.$store.state.SERVER_DATE_FORMAT },
            rendervalue: (value) => {
              return value || '---'
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'createdDate'
          }
        },
        {
          text: this.$t('modifiedBy'),
          value: 'modifiedBy',
          align: 'left',
          sortable: true,
          custom: true,
          rendervalue: (value) => {
            return value || '---'
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'modifiedBy'
          }
        },
        {
          text: this.$t('modifyDateTime'),
          value: 'modifiedDate',
          align: 'left',
          sortable: true,
          render: {
            type: DateColumnComponent,
            props: { serverFormat: this.$store.state.SERVER_DATE_FORMAT },
            rendervalue: (value) => {
              return value || '---'
            }
          },
          filter: {
            useFilter: true,
            isActive: false,
            field: 'modifiedDate'
          }
        },
        {
          text: this.$t('actions'),
          value: '',
          fixed: true,
          sortable: false,
          cellClass: 'pa-1',
          action: true,
          filter: {
            useFilter: false
          }
        }
      ],
      AudioAndVideoHeaders: [
        {
          text: this.$t('audioBitrate'),
          value: 'audioBitrate',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('audioChannels'),
          value: 'audioChannels',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('audioCodec'),
          value: 'audioCodec',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('audioLanguage'),
          value: 'audioLanguage',
          align: 'left',
          sortable: true,
          rendervalue: (value) => {
            return value || '---'
          }
        },
        {
          text: this.$t('audioSampleRate'),
          value: 'audioSampleRate',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('bitrate'),
          value: 'bitrate',
          align: 'left',
          sortable: true,
          render: {
            type: BitrateColumnComponent,
            props: {
              content: this.$t('bitrate'),
              locale: this.$i18n.locale,
              textLenght: 15,
              tooltipPosition: 'is-top'
            }
          }
        },
        {
          text: this.$t('extension'),
          value: 'extension',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('typeMediaLabel'),
          value: 'mediaType',
          align: 'left',
          sortable: true,
          rendervalue: (value) => {
            return value === 1
              ? this.$tc('video', 1)
              : value === 0
              ? this.$tc('audio', 1)
              : this.$tc('imageLabel', 1)
          }
        },
        {
          text: this.$t('typeStreamLabel'),
          value: 'streamType',
          align: 'left',
          sortable: true,
          rendervalue: (value) => {
            return value === 1 ? this.$tc('stored') : this.$tc('streamed')
          }
        },
        {
          text: this.$t('fileSize'),
          value: 'size',
          align: 'left',
          sortable: true,
          render: {
            type: FileSizeColumnComponent,
            props: {
              content: this.$t('fileSize'),
              locale: this.$i18n.locale,
              textLenght: 15,
              tooltipPosition: 'is-top'
            }
          }
        },
        {
          text: this.$t('durationLabel'),
          value: 'duration',
          align: 'left',
          sortable: true,
          custom: true,
          render: {
            type: DurationColumnComponent,
            props: {
              content: this.$t('durationLabel'),
              locale: this.$i18n.locale,
              textLenght: 15,
              tooltipPosition: 'is-top'
            }
          }
        }
      ],
      ImageAndVideoHeaders: [
        {
          text: 'Codec',
          value: 'codec',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('pixelFormat'),
          value: 'pixelFormat',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('aspectRatio'),
          value: 'ratio',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('bitrate'),
          value: 'bitrate',
          align: 'left',
          sortable: true,
          render: {
            type: BitrateColumnComponent,
            props: {
              content: this.$t('bitrate'),
              locale: this.$i18n.locale,
              textLenght: 15,
              tooltipPosition: 'is-top'
            }
          }
        },
        {
          text: this.$t('extension'),
          value: 'extension',
          align: 'left',
          sortable: true
        },
        {
          text: this.$t('typeMediaLabel'),
          value: 'mediaType',
          align: 'left',
          sortable: true,
          rendervalue: (value) => {
            return value === 1
              ? this.$tc('video', 1)
              : value === 0
              ? this.$tc('audio', 1)
              : this.$tc('imageLabel', 1)
          }
        },
        {
          text: this.$t('typeStreamLabel'),
          value: 'streamType',
          align: 'left',
          sortable: true,
          rendervalue: (value) => {
            return value === 1 ? this.$tc('stored') : this.$tc('streamed')
          }
        },
        {
          text: this.$t('fileSize'),
          value: 'size',
          align: 'left',
          sortable: true,
          render: {
            type: FileSizeColumnComponent,
            props: {
              content: this.$t('fileSize'),
              locale: this.$i18n.locale,
              textLenght: 15,
              tooltipPosition: 'is-top'
            }
          }
        },
        {
          text: this.$t('resolutionLabel'),
          value: 'resolution',
          align: 'left',
          sortable: true,
          rendervalue: (value) => {
            return value || '---'
          }
        }
      ],
      FilterFormMedia: {
        MediaFilter: {
          title: this.$t('propertiesLabel'),
          icon: 'television-classic',
          form: {
            MediaTitle: {
              type: TextComponent,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('titleLabel'),
                showClearIcon: true
              },
              operators: ['==', '@=*'],
              activeOperator: '=='
            },
            MediaType: {
              type: SelectComponent,
              default: '',
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('typeMediaLabel'),
                expanded: true,
                placeholder: this.$t('placeholderSelect', {
                  key: this.$t('typeMediaLabel').toLowerCase()
                }),
                optionId: 'id',
                optionName: 'typeName',
                optionItems: [
                  {
                    id: 0,
                    typeName: this.$tc('audio', 1)
                  },
                  {
                    id: 1,
                    typeName: this.$tc('video', 1)
                  }
                ],
                showClearIcon: true
              }
            },
            StreamType: {
              type: SelectComponent,
              default: '',
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('typeStreamLabel'),
                expanded: true,
                placeholder: this.$t('placeholderSelect', {
                  key: this.$t('typeStreamLabel').toLowerCase()
                }),
                value: null,
                optionId: 'id',
                optionName: 'typeName',
                optionItems: [
                  {
                    id: 'Streamed',
                    typeName: this.$t('streamed')
                  },
                  {
                    id: 'Stored',
                    typeName: this.$t('stored')
                  }
                ],
                showClearIcon: true
              }
            }
          }
        },
        DatesFilter: {
          title: this.$tc('date', 2),
          icon: 'calendar',
          form: {
            CreatedDate: {
              type: FullDateTimePickerComponent,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('createDateTime'),
                showClearIcon: true,
                locale: this.$i18n.locale,
                position: 'is-top-left',
                activeOperator: 'date'
              },
              activeOperator: 'date',
              value: null,
              events: {
                operatorUpdated: (value) => {
                  this.filterForm.DatesFilter.form.CreatedDate.activeOperator =
                    value
                  this.filterForm.DatesFilter.form.CreatedDate[
                    'field-input'
                  ].activeOperator = value
                }
              }
            },
            ModifiedDate: {
              type: FullDateTimePickerComponent,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('modifyDateTime'),
                showClearIcon: true,
                locale: this.$i18n.locale,
                position: 'is-top-left',
                activeOperator: 'date'
              },
              activeOperator: 'date',
              value: null,
              events: {
                operatorUpdated: (value) => {
                  this.filterForm.DatesFilter.form.ModifiedDate.activeOperator =
                    value
                  this.filterForm.DatesFilter.form.ModifiedDate[
                    'field-input'
                  ].activeOperator = value
                }
              }
            }
          }
        }
      },
      FilterFormMediaNoTypes: {
        MediaFilter: {
          title: this.$t('propertiesLabel'),
          icon: 'television-classic',
          form: {
            MediaTitle: {
              type: TextComponent,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('titleLabel'),
                showClearIcon: true
              },
              operators: ['==', '@=*'],
              activeOperator: '=='
            }
          }
        },
        DatesFilter: {
          title: this.$tc('date', 2),
          icon: 'calendar',
          form: {
            CreatedDate: {
              type: FullDateTimePickerComponent,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('createDateTime'),
                showClearIcon: true,
                locale: this.$i18n.locale,
                position: 'is-top-left',
                activeOperator: 'date'
              },
              activeOperator: 'date',
              value: null,
              events: {
                operatorUpdated: (value) => {
                  this.filterForm.DatesFilter.form.CreatedDate.activeOperator =
                    value
                  this.filterForm.DatesFilter.form.CreatedDate[
                    'field-input'
                  ].activeOperator = value
                }
              }
            },
            ModifiedDate: {
              type: FullDateTimePickerComponent,
              'field-wrapper': { class: 'is-12' },
              'field-input': {
                isRequired: false,
                label: this.$t('modifyDateTime'),
                showClearIcon: true,
                locale: this.$i18n.locale,
                position: 'is-top-left',
                activeOperator: 'date'
              },
              activeOperator: 'date',
              value: null,
              events: {
                operatorUpdated: (value) => {
                  this.filterForm.DatesFilter.form.ModifiedDate.activeOperator =
                    value
                  this.filterForm.DatesFilter.form.ModifiedDate[
                    'field-input'
                  ].activeOperator = value
                }
              }
            }
          }
        }
      }
    }
  },
  computed: {
    staticAudioPng() {
      return require('static/audio_media.png')
    },
    staticVideoPng() {
      return require('static/video_media.png')
    },
    MediaForm() {
      return {
        media: {
          type: UppyFileUploader,
          default: '',
          'field-wrapper': { class: 'is-12' },
          'field-input': {
            isRequired: true,
            label: this.$t('mediasLabel'),
            placeholder: this.$t('placeholder', {
              key: this.$t('mediasLabel').toLowerCase()
            }),
            showClearIcon: false,
            allowedFileTypes: this.allowedFileTypes,
            tusEndpoint: '/api-data/files',
            readonlyMode: false,
            tooltipSave: this.$t('tooltipSaveMedia'),
            maxNumberOfFiles: undefined,
            metaData: this.metaData
          },
          events: {
            'upload-success': (file) => {
              if (file && typeof file === 'object') {
                this.onMediaUploadComplete(file)
              }
            }
          },
          save: (v) => {
            return (v && v.Name) || v
          }
        },
        mediaId: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          value: ''
        },
        mediaEntityId: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          value: null
        },
        mediaTitle: {
          type: null,
          'field-wrapper': { style: 'display: none' },
          value: ''
        },
        sourcePath: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        duration: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        aspectRatio: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        resolution: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        mediaType: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        },
        mediaDescription: {
          type: null,
          value: null,
          'field-wrapper': { style: 'display: none' },
          'field-input': {
            isRequired: false
          }
        }
      }
    }
  },
  methods: {
    getStaticMediaThumbnail(mediaType) {
      return mediaType === 0
        ? this.staticAudioPng
        : mediaType === 1
        ? this.staticVideoPng
        : ''
    }
  }
}
