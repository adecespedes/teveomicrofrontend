import { DynamicForm } from 'premium-tesh-ui-lib'

import AdvancedCropper from '~/components/Inputs/AdvancedCropper'

export default {
  computed: {
    avatarForm() {
      return {
        avatar: {
          type: AdvancedCropper,
          default: this.user?.avatar,
          'field-wrapper': {
            class: 'is-12'
          },
          'field-input': {
            placeholder: this.$t('placeholder', {
              key: this.$t('avatar').toLowerCase()
            }),
            isRequired: true,
            expanded: true,
            aspectRatio: 4 / 4,
            stencilComponent: 'circle-stencil',
            imageRestriction: 'none'
          },
          save: (v) => {
            return v && v.replace('data:image/png;base64,', '')
          }
        }
      }
    }
  },
  methods: {
    setAvatar() {
      this.$buefy.modal.open({
        parent: this,
        component: DynamicForm,
        props: {
          form: this.avatarForm,
          isInModal: true,
          locale: this.$i18n.locale,
          cardTitle: 'Avatar',
          cardSubtitle: this.$t('editButton'),
          preventSubmit: true
        },
        events: {
          onSubmit: (data) => {
            this.$axios.post(`/Account/SetUserAvatar`, data).then(() => {
              // Update profile Avatar
              this.$store.commit('basic', {
                key: 'userAvatar',
                value: 'data:image/png;base64,' + data.avatar
              })
              const user = { ...this.$store.state.user }
              user.avatar = 'data:image/png;base64,' + data.avatar

              this.$store.commit('basic', {
                key: 'user',
                value: user
              })
            })
          }
        },
        hasModalCard: true,
        destroyOnHide: true,
        trapFocus: true
      })
    }
  }
}
