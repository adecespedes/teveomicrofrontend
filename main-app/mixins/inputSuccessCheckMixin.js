const inputSuccessCheck = {
  methods: {
    inputIsSuccess(valid, value) {
      return (
        valid &&
        value &&
        ((!Array.isArray(value) && value !== '' && value !== 'null') ||
          (Array.isArray(value) && value.length > 0))
      )
    }
  }
}
export default inputSuccessCheck
