import dayjs from 'dayjs'
const customParseFormat = require('dayjs/plugin/customParseFormat')
dayjs.extend(customParseFormat)
const duration = require('dayjs/plugin/duration')
dayjs.extend(duration)
const utc = require('dayjs/plugin/utc')
dayjs.extend(utc)

export default {
  data() {
    return {
      activeFilters: [],
      queries: {},
      filtersConstants: {
        CreatedBy: 'createdBy',
        CreatedDate: 'createdDate',
        ModifiedBy: 'modifiedBy',
        ModifiedDate: 'modifiedDate'
      }
    }
  },
  methods: {
    updateFilters(queries, utc) {
      if (utc) this.queries = { ...this.queries, queries }
      else this.queries = { ...this.queries, ...queries }

      this.filters = this.generateUrlFilters(utc)
    },
    generateUrlFilters(utc) {
      const array = []
      let str = ''

      // Generate standar filters
      str = this.urlFilters(this.queries, utc, this.filtersConstants, 'table')
      array.push(str)

      return array.join(',')
    },
    highlightActiveFilters(headers) {
      if (headers && headers.length) {
        headers.forEach((header) => {
          if (header.filter && header.value) {
            header.filter.isActive = this.activeFilters.length
              ? this.activeFilters.includes(header.value)
              : false
          }
        })
      }
    },
    specificDateFilter(
      dateFilter,
      dateFormFieldName,
      queries,
      utc,
      filtersConstants,
      dateFormat = 'YYYY-MM-DD',
      dateTimeFormat = 'YYYY-MM-DD HH:mm'
    ) {
      let str = ''
      const array = []

      if (utc) {
        if (queries[dateFormFieldName] && queries[dateFormFieldName].value) {
          this.activeFilters.push(filtersConstants[dateFilter])
          if (queries[dateFormFieldName].activeOperator === 'range') {
            const dateTime = queries[dateFormFieldName].value[1]
            str =
              filtersConstants[dateFilter] +
              '>=' +
              dayjs(queries[dateFormFieldName].value[0]).utc().format()
            array.push(str)
            dateTime.setHours(dateTime.getHours() + 24)

            str =
              filtersConstants[dateFilter] +
              '<' +
              dayjs(dateTime).utc().format()
            array.push(str)
          } else if (queries[dateFormFieldName].activeOperator === 'month') {
            const lastDayInMonth = dayjs(
              queries[dateFormFieldName].value
            ).daysInMonth()
            const firstDayDate = dayjs(queries[dateFormFieldName].value)
              .date(1)
              .utc()
              .format()
            const lastDayDate = dayjs(queries[dateFormFieldName].value)
              .date(lastDayInMonth)
              .utc()
              .format()
            str = filtersConstants[dateFilter] + '>=' + firstDayDate
            array.push(str)
            str = filtersConstants[dateFilter] + '<=' + lastDayDate
            array.push(str)
          } else if (
            queries[dateFormFieldName].activeOperator === 'beforeThan'
          ) {
            const dateTime = queries[dateFormFieldName].value
            str =
              filtersConstants[dateFilter] +
              '<' +
              dayjs(dateTime).utc().format()
            array.push(str)
          } else if (
            queries[dateFormFieldName].activeOperator === 'afterThan'
          ) {
            const dateTime = queries[dateFormFieldName].value
            str =
              filtersConstants[dateFilter] +
              '>' +
              dayjs(dateTime).utc().format()
            array.push(str)
          } else if (queries[dateFormFieldName].activeOperator === 'year') {
            const dateTime = queries[dateFormFieldName].value
            str =
              filtersConstants[dateFilter] +
              '>=' +
              dayjs(`${dateTime}-01-01`).utc().format()
            array.push(str)
            str =
              filtersConstants[dateFilter] +
              '<=' +
              dayjs(`${dateTime}-12-31`).utc().format()
            array.push(str)
          } else if (
            queries[dateFormFieldName].activeOperator === 'trimester' ||
            queries[dateFormFieldName].activeOperator === 'summer'
          ) {
            const lastDate = dayjs(queries[dateFormFieldName].value[1])
              .utc()
              .format()
            const firstDate = dayjs(queries[dateFormFieldName].value[0])
              .utc()
              .format()
            str = filtersConstants[dateFilter] + '>=' + firstDate
            array.push(str)
            str = filtersConstants[dateFilter] + '<=' + lastDate
            array.push(str)
          } else {
            str =
              filtersConstants[dateFilter] +
              '>=' +
              dayjs(queries[dateFormFieldName].value).utc().format()
            array.push(str)
          }
          // assumes is a date
        }
      } else if (
        queries[dateFormFieldName] &&
        queries[dateFormFieldName].value
      ) {
        this.activeFilters.push(filtersConstants[dateFilter])
        if (queries[dateFormFieldName].activeOperator === 'range') {
          const dateTime = queries[dateFormFieldName].value[1]
          str =
            filtersConstants[dateFilter] +
            '>=' +
            dayjs(queries[dateFormFieldName].value[0]).format(dateFormat)
          array.push(str)
          dateTime.setHours(dateTime.getHours() + 24)

          str =
            filtersConstants[dateFilter] +
            '<' +
            dayjs(dateTime).format(dateFormat)
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'month') {
          const lastDayInMonth = dayjs(
            queries[dateFormFieldName].value
          ).daysInMonth()
          const firstDayDate = dayjs(queries[dateFormFieldName].value)
            .date(1)
            .format(dateFormat)
          const lastDayDate = dayjs(queries[dateFormFieldName].value)
            .date(lastDayInMonth)
            .format(dateFormat)
          str = filtersConstants[dateFilter] + '>=' + firstDayDate
          array.push(str)
          str = filtersConstants[dateFilter] + '<=' + lastDayDate
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'beforeThan') {
          const dateTime = queries[dateFormFieldName].value
          str =
            filtersConstants[dateFilter] +
            '<' +
            dayjs(dateTime).format(dateTimeFormat)
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'afterThan') {
          const dateTime = queries[dateFormFieldName].value
          str =
            filtersConstants[dateFilter] +
            '>' +
            dayjs(dateTime).format(dateTimeFormat)
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'year') {
          const dateTime = queries[dateFormFieldName].value
          str =
            filtersConstants[dateFilter] +
            '>=' +
            dayjs(`${dateTime}-01-01`).format(dateFormat)
          array.push(str)
          str =
            filtersConstants[dateFilter] +
            '<=' +
            dayjs(`${dateTime}-12-31`).format(dateFormat)
          array.push(str)
        } else if (
          queries[dateFormFieldName].activeOperator === 'trimester' ||
          queries[dateFormFieldName].activeOperator === 'summer'
        ) {
          const lastDate = dayjs(queries[dateFormFieldName].value[1]).format(
            'YYYY-MM-DD HH:mm:ss'
          )
          const firstDate = dayjs(queries[dateFormFieldName].value[0]).format(
            'YYYY-MM-DD HH:mm:ss'
          )
          str = filtersConstants[dateFilter] + '>=' + firstDate
          array.push(str)
          str = filtersConstants[dateFilter] + '<=' + lastDate
          array.push(str)
        } else {
          str =
            filtersConstants[dateFilter] +
            '>=' +
            dayjs(queries[dateFormFieldName].value).format(dateFormat)
          array.push(str)
        }
        // assumes is a date
      }

      return array
    },
    urlFilters(queryParams, utc, filterConstants, resultContainerRef = null) {
      const srtArr = []
      let str = ''
      let pagination = ''

      // Spread standar date filters from normal ones
      const dateQueries = {}

      if (utc) {
        dateQueries.CreatedDate = { ...queryParams.queries.CreatedDate }
        dateQueries.ModifiedDate = { ...queryParams.queries.ModifiedDate }
        delete queryParams.queries.CreatedDate
        delete queryParams.queries.ModifiedDate

        for (const field in queryParams.queries) {
          if (field === 'MaxResults' && queryParams.queries[field]) {
            if (this.$refs.table) {
              this.$refs.table.pagination.itemsPerPage =
                queryParams.queries[field]
            } else if (this.$refs.cardMenu) {
              this.$refs.cardMenu.itemsPerPage = queryParams.queries[field]
            } else if (resultContainerRef && this.$refs[resultContainerRef]) {
              this.$refs[resultContainerRef].itemsPerPage =
                queryParams.queries[field]
            } else pagination = `&pageSize=${queryParams.queries[field]}`
          } else if (
            queryParams.queries[field] &&
            (queryParams.queries[field].value ||
              queryParams.queries[field].value === 0 ||
              queryParams.queries[field].value === false) &&
            ((Array.isArray(queryParams.queries[field].value) &&
              queryParams.queries[field].value.length !== 0) ||
              !Array.isArray(queryParams.queries[field].value))
          ) {
            str = `${
              filterConstants ? filterConstants[field] || field : field // Add field name to filter
            }${queryParams.queries[field].activeOperator}${
              queryParams.queries[field].value
            }` // Add field operator and value for comparison
            srtArr.push(str)
            this.activeFilters.push(
              filterConstants ? filterConstants[field] || field : field
            )
          }
        }
      } else {
        dateQueries.CreatedDate = { ...queryParams.CreatedDate }
        dateQueries.ModifiedDate = { ...queryParams.ModifiedDate }
        delete queryParams.CreatedDate
        delete queryParams.ModifiedDate

        for (const field in queryParams) {
          if (field === 'MaxResults' && queryParams[field]) {
            if (this.$refs.table) {
              this.$refs.table.pagination.itemsPerPage = queryParams[field]
            } else if (this.$refs.cardMenu) {
              this.$refs.cardMenu.itemsPerPage = queryParams[field]
            } else if (resultContainerRef && this.$refs[resultContainerRef]) {
              this.$refs[resultContainerRef].itemsPerPage = queryParams[field]
            } else pagination = `&pageSize=${queryParams[field]}`
          } else if (
            queryParams[field] &&
            (queryParams[field].value ||
              queryParams[field].value === 0 ||
              queryParams[field].value === false) &&
            ((Array.isArray(queryParams[field].value) &&
              queryParams[field].value.length !== 0) ||
              !Array.isArray(queryParams[field].value))
          ) {
            str = `${
              filterConstants ? filterConstants[field] || field : field // Add field name to filter
            }${queryParams[field].activeOperator}${queryParams[field].value}` // Add field operator and value for comparison
            srtArr.push(str)
            this.activeFilters.push(
              filterConstants ? filterConstants[field] || field : field
            )
          }
        }
      }
      // Add dates filters
      Array.prototype.push.apply(
        srtArr,
        this.specificDateFilter(
          'CreatedDate',
          'CreatedDate',
          dateQueries,
          utc,
          filterConstants
        )
      )

      Array.prototype.push.apply(
        srtArr,
        this.specificDateFilter(
          'ModifiedDate',
          'ModifiedDate',
          dateQueries,
          utc,
          filterConstants
        )
      )

      if (pagination.length > 0) srtArr.push(pagination)

      return srtArr.join(',')
    },
    countResults(queries, baseUrl) {
      if (!baseUrl) baseUrl = this.getBasePath
      this.queries = { ...this.queries, ...queries }
      const f = this.generateUrlFilters() || ''
      try {
        const url = `${baseUrl.split('?filters')[0]}?filters=${
          f !== '' ? f + '&' : '&'
        }allRecords=1`
        this.$axios.get(url).then((response) => {
          this.$nuxt.$emit(
            'results-count',
            response.data.records || response.data.length || 0
          )
        })
      } catch (error) {
        this.$nuxt.$emit('results-count', null)
        this.$buefy.snackbar.open({
          message: 'Error: ' + error,
          queue: false
        })
      }
    },
    assignColumnFilterForm(field, filterForm = this.filterForm) {
      if (field && !field.filterForm) {
        let found = false
        const keys = Object.keys(filterForm)
        for (let index = 0; index < keys.length && !found; index++) {
          const k = keys[index]
          const form = filterForm[k].form || filterForm[k].fields || {}
          const array = Object.keys(form).filter(
            (item) =>
              this.filtersConstants[item] === field.field ||
              item === field.field
          )
          array.forEach((element) => {
            found = true
            field.filterForm = {
              [element]: {
                title: field.field,
                form: { [element]: form[element] }
              }
            }
          })
        }
      }
    }
  }
}
