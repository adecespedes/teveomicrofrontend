const es = [
  'abanto',
  'abrazafarolas',
  'adufe',
  'alcornoque',
  'alfeñique',
  'andurriasmo',
  'arrastracueros',
  'artabán',
  'atarre',
  'baboso',
  'barrabás',
  'barriobajero',
  'bebecharcos',
  'bellaco',
  'belloto',
  'berzotas',
  'besugo',
  'bobalicón',
  'bocabuzón',
  'bocachancla',
  'bocallanta',
  'boquimuelle',
  'borrico',
  'botarate',
  'brasas',
  'cabestro',
  'cabezaalberca',
  'cabezabuque',
  'cachibache',
  'cafre',
  'cagalindes',
  'cagarruta',
  'calambuco',
  'calamidad',
  'caldúo',
  'calientahielos',
  'calzamonas',
  'cansalmas',
  'cantamañanas',
  'capullo',
  'caracaballo',
  'caracartón',
  'caraculo',
  'caraflema',
  'carajaula',
  'carajo',
  'carajote',
  'carapapa',
  'carapijo',
  'cazurro',
  'cebollino',
  'cenizo',
  'cenutrio',
  'ceporro',
  'cernícalo',
  'charrán',
  'chiquilicuatre',
  'chirimbaina',
  'chupacables',
  'chupasangre',
  'chupóptero',
  'cierrabares',
  'cipote',
  'comebolsas',
  'comechapas',
  'comeflores',
  'comestacas',
  'comepinga',
  'come pinga',
  'cretino',
  'cuerpoescombro',
  'culopollo',
  'descerebrado',
  'desgarracalzas',
  'dondiego',
  'donnadie',
  'echacantos',
  'ejarramantas',
  'energúmeno',
  'esbaratabailes',
  'escolimoso',
  'escornacabras',
  'estulto',
  'fanfosquero',
  'fantoche',
  'fariseo',
  'filimincias',
  'foligoso',
  'fulastre',
  'ganapán',
  'ganapio',
  'gandúl',
  'gañán',
  'gaznápiro',
  'gilipuertas',
  'gilipollas',
  'giraesquinas',
  'gorrino',
  'gorrumino',
  'guitarro',
  'gurriato',
  'habahelá',
  'huelegateras',
  'huevón',
  'lamecharcos',
  'lameculos',
  'lameplatos',
  'lechuguino',
  'lerdo',
  'letrín',
  'lloramigas',
  'longanizas',
  'lumbreras',
  'maganto',
  'majadero',
  'malasangre',
  'malasombra',
  'malparido',
  'mameluco',
  'mamporrero',
  'manegueta',
  'mangarrán',
  'mangurrián',
  'mastuerzo',
  'matacandiles',
  'meapilas',
  'melón',
  'mendrugo',
  'mentecato',
  'mequetrefe',
  'merluzo',
  'metemuertos',
  'metijaco',
  'mindundi',
  'morlaco',
  'morroestufa',
  'muerdesartenes',
  'orate',
  'ovejo',
  'pagafantas',
  'palurdo',
  'pamplinas',
  'panarra',
  'panoli',
  'papafrita',
  'papanatas',
  'papirote',
  'paquete',
  'pardillo',
  'parguela',
  'pasmarote',
  'pasmasuegras',
  'pataliebre',
  'patán',
  'pavitonto',
  'pazguato',
  'pecholata',
  'pedorro',
  'peinabombillas',
  'peinaovejas',
  'pelagallos',
  'pelagambas',
  'pelagatos',
  'pelatigres',
  'pelazarzas',
  'pelele',
  'pelma',
  'percebe',
  'perrocostra',
  'perroflauta',
  'peterete',
  'petimetre',
  'picapleitos',
  'pichabrava',
  'pillavispas',
  'pPiltrafa',
  'pinchauvas',
  'pintamonas',
  'piojoso',
  'pitañoso',
  'pitofloro',
  'pinga',
  'plomo',
  'pocasluces',
  'pollopera',
  'quitahipos',
  'rastrapajo',
  'rebañasandías',
  'revientabaules',
  'ríeleches',
  'robaperas',
  'sabandija',
  'sacamuelas',
  'sanguijuela',
  'sinentraero',
  'singao',
  'singado',
  'sinsustancia',
  'sonajas',
  'sonso',
  'soplagaitas',
  'soplaguindas',
  'sosco',
  'tagarote',
  'tarado',
  'tarugo',
  'tiralevitas',
  'tocapelotas',
  'tocho',
  'tolai',
  'tontaco',
  'tontucio',
  'tordo',
  'tragaldabas',
  'tuercebotas',
  'tunante',
  'zamacuco',
  'zambombo',
  'zampabollos',
  'zamugo',
  'zángano',
  'zarrapastroso',
  'zascandil',
  'zopenco',
  'zoquete',
  'zote',
  'zullenco',
  'zurcefrenillos'
]
const en = [
  'anal',
  'anus',
  'arse',
  'ass',
  'ballsack',
  'balls',
  'bastard',
  'bitch',
  'biatch',
  'bloody',
  'blowjob',
  'blow job',
  'bollock',
  'bollok',
  'boner',
  'boob',
  'bugger',
  'bum',
  'butt',
  'buttplug',
  'clitoris',
  'cock',
  'coon',
  'crap',
  'cunt',
  'damn',
  'dick',
  'dildo',
  'dyke',
  'fag',
  'feck',
  'fellate',
  'fellatio',
  'felching',
  'fuck',
  'f u c k',
  'fudgepacker',
  'fudge packer',
  'flange',
  'Goddamn',
  'God damn',
  'hell',
  'homo',
  'jerk',
  'jizz',
  'knobend',
  'knob end',
  'labia',
  'lmao',
  'lmfao',
  'muff',
  'nigger',
  'nigga',
  'omg',
  'penis',
  'piss',
  'poop',
  'prick',
  'pube',
  'pussy',
  'queer',
  'scrotum',
  'sex',
  'shit',
  's hit',
  'sh1t',
  'slut',
  'smegma',
  'spunk',
  'tit',
  'tosser',
  'turd',
  'twat',
  'vagina',
  'wank',
  'whore',
  'wtf'
]
const badWordsList = es.concat(en)
export default badWordsList
