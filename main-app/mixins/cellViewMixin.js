export default {
  data() {
    return {
      windowWidth: window.innerWidth
    }
  },
  methods: {
    handleResize() {
      this.windowWidth = window.innerWidth
    }
  },
  computed: {
    cellView() {
      if (this.windowWidth < 768) {
        return true
      }
      return false
    }
  },
  mounted() {
    window.addEventListener('resize', this.handleResize)
    // eslint-disable-next-line no-console
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.handleResize)
  }
}
