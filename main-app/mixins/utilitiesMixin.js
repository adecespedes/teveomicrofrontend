export default {
  data() {
    return {
      formBtns: {
        close: {
          show: true,
          icon: 'close',
          label: this.$i18n.t('cancelButton')
        },
        save: {
          show: this.$auth.loggedIn,
          icon: 'content-save',
          label: this.$i18n.t('saveButton'),
          props: { type: 'is-primary' }
        }
      },
      heartbeatTimer: null
    }
  },
  computed: {
    isMobileOrTablet() {
      return this.$mq === 'mobile' || this.$mq === 'tablet'
    },
    isDesktop() {
      return this.$mq === 'desktop' || this.$mq === 'widescreen'
    },
    isFullHD() {
      return this.$mq === 'fullhd'
    },
    loggedIn() {
      return this.$auth.loggedIn
    },
    user() {
      return this.$auth.$state.user
    },
    username() {
      return (
        (this.$store.state.user && this.$store.state.user.userName) ||
        (this.user && this.user.userName)
      )
    },
    fullname() {
      return (
        (this.$store.state.user && this.$store.state.user.fullName) ||
        (this.user && this.user.fullName)
      )
    },
    isSystemAdmin() {
      return (
        this.user &&
        this.user.userPermissions &&
        this.user.userPermissions.systemPermissions &&
        this.user.userPermissions.systemPermissions.includes('SystemAdmin')
      )
    },
    isSystemGlobal() {
      return (
        this.user &&
        this.user.userPermissions &&
        this.user.userPermissions.systemPermissions &&
        this.user.userPermissions.systemPermissions.includes('AccessAll')
      )
    },
    userSystemPermissions() {
      return this.user &&
        this.user.userPermissions &&
        this.user.userPermissions.systemPermissions
        ? [...this.user.userPermissions.systemPermissions]
        : []
    },
    userPermissionsByDomain() {
      return this.user &&
        this.user.userPermissions &&
        this.user.userPermissions.userPermissionsByDomain
        ? [...this.user.userPermissions.userPermissionsByDomain]
        : []
    }
  },
  methods: {
    loadFormData(form, data) {
      if (form && typeof form === 'object' && data) {
        for (const key in form) {
          if (Object.hasOwnProperty.call(data, key)) {
            form[key].value = data[key]
          }
        }
      }
    },
    goBack() {
      this.$router.go(-1)
    },
    userHasRole(roleName) {
      let roles = []
      if (this.$auth.$state.user) {
        roles =
          this.$auth.$state.user.userRolesAndPermissions &&
          this.$auth.$state.user.userRolesAndPermissions.userRoles
      }

      let rolList = []
      if (roles && roles.length) rolList = roles.map((r) => r.name)

      return this.$auth.$state.user
        ? rolList.length && rolList.includes(roleName)
        : false
    },
    getUserPermissionByDomain(domainId) {
      const permissions = this.userPermissionsByDomain.find(
        (perm) => perm.domainId === domainId
        // (perm) => perm === domainId
      )
      return permissions ? permissions.permissions : []
    },
    parseServerURL(path) {
      return path && (path.includes('http') || path.includes('https'))
        ? path
        : path
        ? this.$store.state.APP_BASE_URL +
          this.$store.state.API_DATA_PREFIX +
          path
        : null
    },
    getFallbackImage(name = 'fallback', isAvatar = false) {
      return `https://avatars.dicebear.com/v2/${
        isAvatar ? 'human' : 'jdenticon'
      }/${name}.svg`
    },
    resfreshHeartBeat(publicationId) {
      this.$axios
        .post('/PublicationHeartbeat', { publicationId })
        .then((response) => {})
        .catch((err) => {
          // eslint-disable-next-line no-console
          console.error(err)
        })
    },
    updateHeartBeat(publicationId, clear = false) {
      // Get info about heartbeat actualization period
      const actRate = 15000
      if (clear) {
        clearInterval(this.heartbeatTimer)
        this.heartbeatTimer = null
      } else if (publicationId) {
        if (!this.heartbeatTimer) {
          this.resfreshHeartBeat(publicationId)
          this.heartbeatTimer = setInterval(() => {
            this.resfreshHeartBeat(publicationId)
          }, actRate)
        }
      }
    }
  }
}
