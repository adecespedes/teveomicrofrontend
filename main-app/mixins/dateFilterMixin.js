import dayjs from 'dayjs'
const customParseFormat = require('dayjs/plugin/customParseFormat')
dayjs.extend(customParseFormat)
const duration = require('dayjs/plugin/duration')
dayjs.extend(duration)

export default {
  methods: {
    specificDateFilter(
      dateFilter,
      dateFormFieldName,
      queries,
      filtersConstants,
      dateFormat = 'YYYY-MM-DD'
    ) {
      let str = ''
      const array = []

      if (queries[dateFormFieldName] && queries[dateFormFieldName].value) {
        if (queries[dateFormFieldName].activeOperator === 'date') {
          str =
            filtersConstants[dateFilter] +
            '>=' +
            dayjs(queries[dateFormFieldName].value).format(dateFormat)
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'range') {
          const dateTime = queries[dateFormFieldName].value[1]
          str =
            filtersConstants[dateFilter] +
            '>=' +
            dayjs(queries[dateFormFieldName].value[0]).format(dateFormat)
          array.push(str)
          dateTime.setHours(dateTime.getHours() + 24)

          str =
            filtersConstants[dateFilter] +
            '<' +
            dayjs(dateTime).format(dateFormat)
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'month') {
          const lastDayInMonth = dayjs(
            queries[dateFormFieldName].value
          ).daysInMonth()
          const firstDayDate = dayjs(queries[dateFormFieldName].value)
            .date(1)
            .format(dateFormat)
          const lastDayDate = dayjs(queries[dateFormFieldName].value)
            .date(lastDayInMonth)
            .format(dateFormat)
          str = filtersConstants[dateFilter] + '>=' + firstDayDate
          array.push(str)
          str = filtersConstants[dateFilter] + '<=' + lastDayDate
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'beforeThan') {
          const dateTime = queries[dateFormFieldName].value
          str =
            filtersConstants[dateFilter] +
            '<' +
            dayjs(dateTime).format('YYYY-MM-DD HH:mm')
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'afterThan') {
          const dateTime = queries[dateFormFieldName].value
          str =
            filtersConstants[dateFilter] +
            '>' +
            dayjs(dateTime).format('YYYY-MM-DD HH:mm')
          array.push(str)
        } else if (queries[dateFormFieldName].activeOperator === 'year') {
          const dateTime = queries[dateFormFieldName].value
          str =
            filtersConstants[dateFilter] +
            '>=' +
            dayjs(`${dateTime}-01-01`).format(dateFormat)
          array.push(str)
          str =
            filtersConstants[dateFilter] +
            '<=' +
            dayjs(`${dateTime}-12-31`).format(dateFormat)
          array.push(str)
        } else if (
          queries[dateFormFieldName].activeOperator === 'trimester' ||
          queries[dateFormFieldName].activeOperator === 'summer'
        ) {
          const lastDate = dayjs(queries[dateFormFieldName].value[1]).format(
            'YYYY-MM-DD HH:mm:ss'
          )
          const firstDate = dayjs(queries[dateFormFieldName].value[0]).format(
            'YYYY-MM-DD HH:mm:ss'
          )
          str = filtersConstants[dateFilter] + '>=' + firstDate
          array.push(str)
          str = filtersConstants[dateFilter] + '<=' + lastDate
          array.push(str)
        }
      }
      return array
    }
  }
}
