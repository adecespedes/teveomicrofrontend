export const state = () => ({
  /* User */
  user: null,
  userEmail: null,
  userAvatar: null,

  // Token Expiration Date
  tokenExpiration: null,
  sessionKey: null,
  /* NavBar */
  isNavBarVisible: true,

  /* FooterBar */
  isFooterBarVisible: true,

  /* Aside */
  isAsideVisible: true,
  isAsideMobileExpanded: false,

  /* Dark mode */
  isDarkModeActive: false,

  /* Url base for api */
  APP_BASE_URL: '',
  API_DATA_PREFIX: '',
  API_IDENTITY_PREFIX: '',
  LOGO_PREFIX: '',

  /* Configuration Data */
  SERVER_DATE_FORMAT: 'MM/DD/YYYY hh:mm:ss',
  APP_IMAGE_URL: '/api-data/files/images/',
  APP_AUDIO_URL: '/api-data/files/audios/',
  APP_VIDEO_URL: '/api-data/files/videos/',

  /* Configurable URLs */
  APP_HEALTH_URL: '',

  /* App language */
  appLocale: 'es',

  /* Users Rol and Permission */
  userRoles: null,

  userPermissions: null,

  // Track current CChannel on edition to save requests
  cChannelOnEdition: null,

  // Track current Publication on edition to save requests
  publicationOnEdition: null,

  // Uploaded Media Progress Info
  mediaInfo: null,

  // Uploaded Media Convertion Info
  mediaConvertionInfo: null,

  // Home search param to filter Publications
  searchCriteria: null,

  // System Internet Connection State
  isInternetConnected: false
})

export const mutations = {
  /* A fit-them-all commit */
  basic(state, payload) {
    state[payload.key] = payload.value
  },

  /* Aside Mobile */
  asideMobileStateToggle(state, payload = null) {
    const htmlClassName = 'has-aside-mobile-expanded'

    let isShow

    if (payload !== null) {
      isShow = payload
    } else {
      isShow = !state.isAsideMobileExpanded
    }

    if (isShow) {
      document.documentElement.classList.add(htmlClassName)
    } else {
      document.documentElement.classList.remove(htmlClassName)
    }

    state.isAsideMobileExpanded = isShow
  },

  /* Dark Mode */
  darkModeToggle(state, payload = null) {
    const htmlClassName = 'is-dark-mode-active'

    state.isDarkModeActive = !state.isDarkModeActive

    if (state.isDarkModeActive) {
      document.documentElement.classList.add(htmlClassName)
    } else {
      document.documentElement.classList.remove(htmlClassName)
    }
  },

  /* Aside Mobile */
  defineApiUrl(state, payload = null) {
    state.APP_BASE_URL = payload
  },

  defineApiPrefix(state, payload = null) {
    state.API_DATA_PREFIX = payload
  },

  defineIdentityApiUrl(state, payload = null) {
    state.API_IDENTITY_PREFIX = payload
  },

  defineLogoUrl(state, payload = null) {
    state.LOGO_PREFIX = payload
  },

  defineServerDateFormat(state, payload = null) {
    state.SERVER_DATE_FORMAT = payload
  },

  defineAppHealthUrl(state, payload = null) {
    state.APP_HEALTH_URL = payload
  },

  // Defines all Medias storage URL
  defineImagesUrl(state, payload = null) {
    state.APP_IMAGE_URL = payload
  },

  defineVideosUrl(state, payload = null) {
    state.APP_VIDEO_URL = payload
  },

  defineAudiosUrl(state, payload = null) {
    state.APP_AUDIO_URL = payload
  },

  defineAppLocale(state, payload = null) {
    state.appLocale = payload
  },

  defineAppUser(state, payload = null) {
    state.user = payload
  },

  defineAppUserEmail(state, payload = null) {
    state.userEmail = payload
  },

  defineAppUserAvatar(state, payload = null) {
    state.userAvatar = payload
  },

  defineTokenExpiration(state, payload = null) {
    state.tokenExpiration = payload
  },

  /* Users Rol and Permission Commit */
  defineUserRoles(state, payload = null) {
    state.userRoles = payload
  },
  defineUserPermissions(state, payload = null) {
    state.userPermissions = payload
  },
  updateCChannelOnEdition(state, payload = null) {
    state.cChannelOnEdition = payload
  },
  updatePublicationOnEdition(state, payload = null) {
    state.publicationOnEdition = payload
  },
  updateMediaProgressInfo(state, payload = null) {
    state.mediaInfo = payload
  },
  updateMediaConvertionInfo(state, payload = null) {
    state.mediaConvertionInfo = payload
  },

  setIsInternetConnected(state, payload = null) {
    state.isInternetConnected = payload
  }
}

export const getters = {
  getKey(key, state) {
    return state[key]
  },

  baseApiUrl(state) {
    return state.APP_BASE_URL
  },

  baseIdentityApiUrl(state) {
    return state.API_IDENTITY_PREFIX
  },

  baseLogoUrl(state) {
    return state.LOGO_PREFIX
  },

  baseServerDateFormat(state) {
    return state.SERVER_DATE_FORMAT
  },

  appHealthUrl(state) {
    return state.APP_HEALTH_URL
  },

  userIsAdmin(state) {
    return (
      state.userRoles &&
      (state.userRoles.includes('Administrator') ||
        state.userRoles.includes('Administrador') ||
        state.userRoles.includes('Admin'))
    )
  },

  userRoles(state) {
    return state.userRoles || []
  },

  cChannelOnEdition(state) {
    return state.cChannelOnEdition
  },

  publicationOnEdition(state) {
    return state.publicationOnEdition
  },
  mediaProgressInfo(state) {
    return state.mediaProgressInfo
  },
  mediaConvertionInfo(state) {
    return state.mediaConvertionInfo
  },

  getIsInternetConnected(state) {
    return state.isInternetConnected
  }
}
