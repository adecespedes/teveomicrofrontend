import Oauth2Scheme from '@nuxtjs/auth-next/dist/schemes/oauth2'
import {
  encodeQuery,
  getResponseProp,
  normalizePath,
  parseQuery
} from '@nuxtjs/auth-next/dist/utils'

export default class CustomScheme extends Oauth2Scheme {
  constructor($auth, options, ...defaults) {
    options.endpoints =
      $auth.$storage.getUniversal('identity.options')?.endpoints
    options.logoutRedirectUri =
      $auth.$storage.getUniversal('identity.options')?.logoutRedirectUri
    super($auth, options, ...defaults)
  }

  async login(_opts = {}) {
    this.options.endpoints = _opts.endpoints
    this.options.logoutRedirectUri = _opts.logoutRedirectUri
    await super.login()
  }

  async mounted() {
    if (this.options.endpoints) {
      await super.mounted()
    }
  }

  _setIdToken(token) {
    const _key = '_id_token.' + this.name

    return this.$auth.$storage.setUniversal(_key, token)
  }

  async _handleCallback() {
    // Handle callback only for specified route
    if (
      this.$auth.options.redirect &&
      normalizePath(this.$auth.ctx.route.path) !==
        normalizePath(this.$auth.options.redirect.callback)
    ) {
      return
    }
    // Callback flow is not supported in server side
    if (process.server) {
      return
    }

    const hash = parseQuery(this.$auth.ctx.route.hash.substr(1))
    const parsedQuery = Object.assign({}, this.$auth.ctx.route.query, hash)
    // accessToken/idToken
    let token = parsedQuery[this.options.token.property]
    // refresh token
    let refreshToken = parsedQuery[this.options.refreshToken.property]
    let idToken = parsedQuery.id_token

    // Validate state
    const state = this.$auth.$storage.getUniversal(this.name + '.state')
    this.$auth.$storage.setUniversal(this.name + '.state', null)
    if (state && parsedQuery.state !== state) {
      return
    }

    // -- Authorization Code Grant --
    if (this.options.responseType === 'code' && parsedQuery.code) {
      let codeVerifier

      // Retrieve code verifier and remove it from storage
      if (
        this.options.codeChallengeMethod &&
        this.options.codeChallengeMethod !== 'implicit'
      ) {
        codeVerifier = this.$auth.$storage.getUniversal(
          this.name + '.pkce_code_verifier'
        )
        this.$auth.$storage.setUniversal(
          this.name + '.pkce_code_verifier',
          null
        )
      }

      const response = await this.$auth.request({
        method: 'post',
        url: this.options.endpoints.token,
        baseURL: '',
        data: encodeQuery({
          code: parsedQuery.code,
          client_id: this.options.clientId,
          redirect_uri: this._redirectURI,
          response_type: this.options.responseType,
          audience: this.options.audience,
          grant_type: this.options.grantType,
          code_verifier: codeVerifier
        })
      })

      token = getResponseProp(response, this.options.token.property) || token
      refreshToken =
        getResponseProp(response, this.options.refreshToken.property) ||
        refreshToken
      idToken = getResponseProp(response, 'id_token') || idToken
    }

    if (!token || !token.length) {
      return
    }

    // Set token
    this.token.set(token)

    // Store refresh token
    if (refreshToken && refreshToken.length) {
      this.refreshToken.set(refreshToken)
    }

    if (idToken && idToken.length) {
      this._setIdToken(idToken)
    }

    // Redirect to home
    this.$auth.redirect('home', true)

    return true // True means a redirect happened
  }

  logout() {
    if (this.options.endpoints.logout) {
      const _key = '_id_token.' + this.name

      const idToken = this.$auth.$storage.getUniversal(_key)
      const opts = {
        id_token_hint: idToken,
        post_logout_redirect_uri: this._logoutRedirectURI
      }
      const url = this.options.endpoints.logout + '?' + encodeQuery(opts)
      window.location.replace(url)
    }
    return this.$auth.reset()
  }
}
