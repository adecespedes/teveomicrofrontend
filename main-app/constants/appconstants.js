export const APP_CONSTANTS = {
  APP_NAME: 'teveo',
  APP_VERSION: 'v1.0.0',
  APP_PORT: 4200,
  APP_DESCRIPTION: 'teveo System',
  APP_NAME_EXTENDED: 'teveo',
  APP_HOME: '/home',
  APP_CLIENT_ID: 'TeVeoFrontend',
  APP_SCOPE: 'TeVeoApi',
  APP_ROLE_ADMIN: 'Administrator',
  APP_NAME_FOR_IDENTITY: 'TeVeo'
}
