export default ({ route, app, from, redirect }) => {
  const adminRequired = route.meta.some((meta) => meta.requiresAdmin)
  let isAdmin = false
  if (app.$auth.$state.loggedIn) {
    let perms = []
    if (app.$auth.$state.user) {
      perms =
        (app.$auth.$state.user.userPermissions &&
          app.$auth.$state.user.userPermissions.systemPermissions) ||
        []
    }

    isAdmin = perms.includes('AccessAll') || perms.includes('SystemAdmin')
  }
  if (adminRequired && !isAdmin) {
    try {
      redirect(201, '/')
    } catch (error) {
      // error
    }
  }
}
