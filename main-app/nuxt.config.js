import { APP_CONSTANTS } from './constants/'
import i18n from './plugins/i18n'

export default {
  // Skip telemetry
  telemetry: { enabled: true, consent: true },
  publicRuntimeConfig: {
    appName: APP_CONSTANTS.APP_NAME,
    appVersion: APP_CONSTANTS.APP_VERSION,
    appRoleAdmin: APP_CONSTANTS.APP_ROLE_ADMIN,
    appHomeUrl: APP_CONSTANTS.APP_HOME
  },
  router: {
    middleware: ['navigation-guard-admin']
  },
  ssr: false,
  server: {
    port: APP_CONSTANTS.APP_PORT // default: 3000
  },
  target: 'static',
  /*
   ** Headers of the page
   */
  head: {
    title: APP_CONSTANTS.APP_NAME || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: APP_CONSTANTS.APP_DESCRIPTION || ''
      },
      {
        hid: 'http-equiv',
        'http-equiv': 'Content-Security-Policy',
        content: 'upgrade-insecure-requests'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#71c737' },
  /*
   ** Global CSS
   */
  css: ['./assets/scss/main.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/premium-tesh-ui-lib.js', mode: 'client' },
    { src: '~/plugins/after-each.js', mode: 'client' },
    { src: '~/plugins/axios.js', mode: 'client' },
    { src: '~/plugins/cookies-functionalities.js', mode: 'client' },
    { src: '~/plugins/root-application.client.js' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/composition-api/module',
    '@nuxtjs/device',
    'nuxt-compress'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    ['nuxt-buefy', { materialDesignIcons: false }],
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/pwa',
    [
      'nuxt-i18n',
      {
        defaultLocale: 'es',
        locales: ['es', 'en'],
        strategy: 'no_prefix',
        vueI18n: i18n,
        vueI18nLoader: true,
        detectBrowserLanguage: false,
        parsePages: false,
        silentTranslationWarn: true,
        silentFallbackWarn: true
      }
    ],
    [
      'nuxt-mq',
      {
        // Default breakpoint for SSR
        defaultBreakpoint: 'mobile',
        breakpoints: {
          mobile: 769,
          tablet: 1024,
          desktop: 1216,
          widescreen: 1408,
          fullhd: Infinity
        }
      }
    ],
    'vue-scrollto/nuxt',
    'vue-social-sharing/nuxt',
    [
      '@nuxtjs/robots',
      {
        robots: {
          UserAgent: '*',
          Disallow: '/'
        }
      }
    ]
  ],
  axios: {
    proxy: true // Can be also an object with default options
  },

  proxy: {
    '/api-data/': 'https://teveo.premiumtesh.nat.cu'
  },
  pwa: {
    meta: {
      title: APP_CONSTANTS.APP_NAME,
      author: 'PremiumTesh'
    },
    manifest: {
      name: APP_CONSTANTS.APP_NAME_EXTENDED,
      short_name: APP_CONSTANTS.APP_NAME,
      lang: 'en',
      display: 'standalone'
    }
  },
  auth: {
    redirect: {
      home: APP_CONSTANTS.APP_HOME,
      login: '/login3',
      callback: '/callback',
      logout: APP_CONSTANTS.APP_HOME
    },
    strategies: {
      google: {
        clientId:
          '919795079496-kmg0b2gdeqq3vobla2ik3p9d0fmrsp0o.apps.googleusercontent.com',
        scope: ['profile', 'email'],
        codeChallengeMethod: '',
        responseType: 'token id_token'
      },
      facebook: {
        endpoints: {
          userInfo:
            'https://graph.facebook.com/v6.0/me?fields=id,name,picture{url}'
        },
        clientId: '402778641951124',
        // clientSecret:'19e6c57c8642cb0a58fadaffe05c5705',
        // callback: 'http://localhost:4200/callback/',
        scope: ['public_profile', 'email']
      },
      discord: {
        clientId: '1026553435081953350',
        clientSecret: ''
      },
      twitch: {
        scheme: 'oauth2',
        endpoints: {
          authorization: 'https://id.twitch.tv/oauth2/authorize',
          token: 'https://id.twitch.tv/oauth2/token',
          userInfo: 'https://api.twitch.tv/kraken/user'
        },
        token: {
          property: 'access_token',
          type: 'Bearer',
          maxAge: 3600
        },
        refreshToken: {
          property: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30
        },
        responseType: 'code',
        grantType: 'authorization_code',
        accessType: 'offline',
        redirectUri: 'http://localhost:4200/callback', // http://localhost:3000/api/twitch/callback
        clientId: 'HeReIsMyClIeNtId',
        scope: ['openid', 'user:read:email', 'user_read'],
        state: process.env.TWITCH_STATE_SECRET
      },
      // identity: {
      //   scheme: '~/schemes/identity',
      //   token: {
      //     property: 'access_token',
      //     type: 'Bearer',
      //     maxAge: 1800
      //   },
      //   refreshToken: {
      //     property: 'refresh_token',
      //     maxAge: 60 * 60 * 24 * 30
      //   },
      //   responseType: 'code',
      //   grantType: 'authorization_code',
      //   accessType: 'offline',
      //   // ******** change this for your Application (Client) ID ********
      //   clientId: APP_CONSTANTS.APP_CLIENT_ID,
      //   codeChallengeMethod: 'S256',
      //   scope: [
      //     'openid',
      //     'profile',
      //     'email',
      //     APP_CONSTANTS.APP_SCOPE,
      //     'PremiumTeshId_api',
      //     'offline_access',
      //     'roles'
      //   ],
      //   autoLogout: true
      // }
      refresh: {
        token: {
          property: 'userTokensInfoRespDTO.token',
          global: true
          // required: true,
          // type: 'Bearer'
        },
        refreshToken: {
          property: 'userTokensInfoRespDTO.token',
          data: 'token',
          maxAge: 60 * 60 * 24 * 7,
          required: false,
          tokenRequired: true
        },
        user: {
          property: false
          // autoFetch: true
        },
        endpoints: {
          login: { url: '/Account/LoginUser', method: 'post' },
          refresh: { url: '/Account/RefreshToken', method: 'post' },
          logout: { url: '/Account/LogoutUser', method: 'post' },
          user: {
            url: '/Account/WhoAmI',
            method: 'get',
            propertyName: false
          }
        }
      }
    },
    plugins: ['~/plugins/base-path.js', '~/plugins/signalR-notifications.js']
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    // analyze: true,
    transpile: ['@nuxtjs/auth'],
    extractCSS: true,
    babel: {
      compact: true
    },
    extend(config, ctx) {
      config.resolve.alias.vue = 'vue/dist/vue.common'
    }
  }
}
