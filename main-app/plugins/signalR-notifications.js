const signalr = require('@microsoft/signalr')

export default ({ app, $auth }) => {
  const connection = new signalr.HubConnectionBuilder()
    .withUrl(
      `${app.store.state.APP_BASE_URL}${app.store.state.API_DATA_PREFIX}/PublicationNotifications`
    )
    .configureLogging(signalr.LogLevel.Information)
    .build()

  connection.on('Connected', (message) => {
    // eslint-disable-next-line no-console
    console.info('Connected to SignalR Hub.', message)
  })

  connection.on('Disconnected', (message) => {
    // eslint-disable-next-line no-console
    console.warn('Disconnected from SignalR Hub.', message)
    setTimeout(() => {
      start()
    }, 2000)
  })

  // with reconnect capability (async/await, not IE11 compatible)
  function start() {
    connection
      .start()
      .then(() => {})
      .catch(function (err) {
        setTimeout(() => {
          start()
        }, 5000)
        // eslint-disable-next-line no-console
        console.error(err)
      })
  }

  connection.onclose(() => {
    // eslint-disable-next-line no-console
    console.log('SignalR Attempting OnClose.', 'Reconnect with Publication Hub')
    start()
  })

  // Connect Publications Notifications
  connection.on('PublicationLikes', function (publicationId, likes, dislikes) {
    window.$nuxt.$emit('publication-likes-dislikes', {
      publicationId,
      likes,
      dislikes
    })
  })

  connection.on(
    'PublicationReproduction',
    function (publicationId, views, lastReproductionDate) {
      window.$nuxt.$emit('publication-reproductions', {
        publicationId,
        views,
        lastReproductionDate
      })
    }
  )

  connection.on('PublicationViews', function (publicationId, views) {
    window.$nuxt.$emit('publication-views', {
      publicationId,
      views
    })
  })

  connection.on('PublicationAreViewing', function (publicationId, areViewing) {
    window.$nuxt.$emit('publication-are-viewing', {
      publicationId,
      areViewing
    })
  })

  setTimeout(() => {
    start()
  }, 8000)
}
