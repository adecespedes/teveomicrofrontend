export default async ({ app }, inject) => {
  try {
    let currentSession = app.cookies.getCookie('sessionKey')
    if (!currentSession) {
      const data = await app.$axios.get(`SessionKey`)
      currentSession = data
      app.cookies.setCookie('sessionKey', currentSession, { 'max-age': 3600 })
      app.store.commit('basic', { key: 'sessionKey', value: currentSession })
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message)
  }
}
