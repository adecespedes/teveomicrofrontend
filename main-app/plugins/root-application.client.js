/* eslint-disable */
import { registerApplication, start, getAppNames } from 'single-spa'

registerApplication({
  name: 'users',
  app: async () => await import('../users/_nuxt/subapp'),
  activeWhen: '/users'
})

start()
