export default async function ({ $axios }) {
  try {
    const data = await $axios.get(`${window.location.origin}/config.json`)
    // Set axios configurations
    $axios.setBaseURL(`${data.data.API_DATA_PREFIX}`)
    $axios.setHeader('Access-Control-Allow-Origin', '*')
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e.message)
  }
}
