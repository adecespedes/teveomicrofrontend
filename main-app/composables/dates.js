const dayjs = require('dayjs')

const today = () => {
  return dayjs().format('YYYY-MM-DD')
}

const yesterday = () => {
  return dayjs().subtract(1, 'day').format('YYYY-MM-DD')
}

export { today, yesterday }
